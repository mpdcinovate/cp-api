require("dotenv").config();
const jwt = require('jsonwebtoken')

module.exports = function generateJwt(user) {
    const accessToken = jwt.sign(user, process.env.ACCESS_TOKEN_SECRET)
    return {accessToken, user};
}
