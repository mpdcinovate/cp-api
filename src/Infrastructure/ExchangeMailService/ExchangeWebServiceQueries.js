module.exports = class ExchangeWebServiceQueries {
    constructor() {
    }

    async getInboxFolder(ews, ewsFunction) {

        const ewsArgs = {
            'FolderShape': {
                'BaseShape': 'Default'
            },
            'FolderIds': {
                'DistinguishedFolderId': {
                    'attributes': {
                        'Id': 'inbox'
                    }
                }
            }
        }

        try {
            const response = await ews.run(ewsFunction, ewsArgs)
            return response.ResponseMessages?.GetFolderResponseMessage?.Folders?.Folder
        }
        catch (e) {
            throw Error(e.toString());
        }
    }

    async findItems(ews, ewsFunction) {
        const ewsArgs = {
            'attributes': {
                'Traversal': 'Shallow'
            },
            'ItemShape': {
                'BaseShape': 'IdOnly'
            },
            'ParentFolderIds': {
                'DistinguishedFolderId': {
                    'attributes': {
                        'Id': 'inbox'
                    }
                }
            }

        }
        const response = await ews.run(ewsFunction, ewsArgs)
        return response.ResponseMessages.FindItemResponseMessage.RootFolder.Items.Message
    }

    async getItem(ews, ewsFunction, messagesId) {
        let ewsArgs, k = [];

        let id = messagesId.ItemId.attributes.Id;
        let changeKey = messagesId.ItemId.attributes.ChangeKey;

        ewsArgs = {
            'ItemShape': {
                'BaseShape': 'Default',
                'IncludeMimeContent': 'false',
                'BodyType': 'Text',
                'AdditionalProperties': {
                    'FieldURI': {
                        'attributes': {
                            'FieldURI': 'item:Attachments'
                        }
                    }
                }
            },

            'ItemIds': {
                'ItemId': {
                    'attributes': {
                        'Id': id,
                        'ChangeKey': changeKey
                    }
                }
            }
        }

        return await ews.run(ewsFunction, ewsArgs)
    }

    async getAttachments(ews, ewsFunction, messageId) {
        let ewsArgs;

        ewsArgs = {
            'AttachmentShape': {
                'BodyType': 'HTML',
            },
            'AttachmentIds': {
                'AttachmentId': {
                    'attributes': {
                        'Id': messageId
                    }
                }
            }
        }

        const response = await ews.run(ewsFunction, ewsArgs)

        return response.ResponseMessages.GetAttachmentResponseMessage.Attachments.FileAttachment
    }

    async markAsRead(ews, folderIdAtributes, ewsFunction) {
        const ewsArgs = {
            'ReadFlag': true,
            'SuppressReadReceipts': true,
            'FolderIds': {
                'FolderId': {
                    'attributes': {
                        'Id': folderIdAtributes.id,
                        'ChangeKey': folderIdAtributes.changeKey
                    }
                }

            }
        }
        await ews.run(ewsFunction, ewsArgs)
    }
}
