const fs = require('fs')
const ExchangeWebServiceQueries = require('./ExchangeWebServiceQueries')

module.exports = class ExchangeWebServiceMessageBuilder {
    constructor() {
    }

    async buildMessage(req, res, db, ews) {
        const exchangeWebServiceQueries = new ExchangeWebServiceQueries();

        let folderProperties;
        //The getFolder method retrieves properties of @portmaputo Inbox Folder
        try{
            folderProperties = await exchangeWebServiceQueries.getInboxFolder(ews, 'GetFolder');
        }
        catch (e) {
            throw Error(e.toString());
        }

        //The GetFolder method returns the number of unread thing (messages in this case!)
        let unreadCount = folderProperties.UnreadCount

        //These attributes are necessary to mark as seen all the messages after return it
        const folderIdAtributes = {
            id: folderProperties.FolderId.attributes.Id,
            changeKey: folderProperties.FolderId.attributes.ChangeKey
        }

        //The findItems method is used to fetch IDs of each messages found on
        //email inbox so that we can open them
        const messagesIds = await exchangeWebServiceQueries.findItems(ews, 'FindItem');

        if (unreadCount !== 0) {
            for (const messageId of messagesIds) {
                const resultOfGettingItem = await exchangeWebServiceQueries.getItem(ews, 'GetItem', messageId)

                if (!resultOfGettingItem.ResponseMessages.GetItemResponseMessage.Items.Message.IsRead) {
                    let subject, body, received_at, fromName, fromEmail;

                    //Building the message composition (to save on Database)
                    subject = resultOfGettingItem.ResponseMessages.GetItemResponseMessage.Items.Message.Subject
                    body = resultOfGettingItem.ResponseMessages.GetItemResponseMessage.Items.Message.Body['$value']
                    received_at = resultOfGettingItem.ResponseMessages.GetItemResponseMessage.Items.Message.DateTimeCreated
                    fromName = resultOfGettingItem.ResponseMessages.GetItemResponseMessage.Items.Message.From.Mailbox.Name
                    fromEmail = resultOfGettingItem.ResponseMessages.GetItemResponseMessage.Items.Message.From.Mailbox.EmailAddress

                    const inserteMessageId = await db('inbox')
                        .insert({
                            subject: resultOfGettingItem.ResponseMessages.GetItemResponseMessage.Items.Message.Subject,
                            body: resultOfGettingItem.ResponseMessages.GetItemResponseMessage.Items.Message.Body['$value'],
                            received_at: resultOfGettingItem.ResponseMessages.GetItemResponseMessage.Items.Message.DateTimeCreated,
                            fromName: resultOfGettingItem.ResponseMessages.GetItemResponseMessage.Items.Message.From.Mailbox.Name,
                            fromEmail: resultOfGettingItem.ResponseMessages.GetItemResponseMessage.Items.Message.From.Mailbox.EmailAddress,
                            seen: false
                        })
                        .returning('id')

                    let messageWithAttachment = ''
                    if (resultOfGettingItem.ResponseMessages.GetItemResponseMessage.Items.Message.HasAttachments) {
                        messageWithAttachment = {
                            messageId: resultOfGettingItem.ResponseMessages.GetItemResponseMessage.Items.Message.ItemId.attributes.Id,
                            attachmentProps: resultOfGettingItem.ResponseMessages.GetItemResponseMessage.Items.Message.Attachments.FileAttachment,
                            hasAttachments: true,
                            idToAttachment: inserteMessageId[0]
                        }
                    }

                    if (messageWithAttachment !== '') {
                        if (messageWithAttachment.attachmentProps.AttachmentId) {

                            let attachmentResult = await exchangeWebServiceQueries.getAttachments(ews, 'GetAttachment', messageWithAttachment.attachmentProps.AttachmentId.attributes.Id)
                            let fileName = `${Date.now()}-${attachmentResult.Name}`
                            let fileContent = attachmentResult.Content

                            //storing the attachment name on database
                            await db('inboxAttachments').insert({
                                name: fileName,
                                inboxId: messageWithAttachment.idToAttachment
                            })

                            fs.writeFile(`./uploads/${fileName}`, fileContent, { encoding: 'base64' }, function (err) {
                                console.log(err);
                            });
                        }
                        //when there are more than one attachment
                        else {
                            for (const attProp of messageWithAttachment.attachmentProps) {
                                let attachmentResult = await exchangeWebServiceQueries.getAttachments(ews, 'GetAttachment', attProp.AttachmentId.attributes.Id)

                                let fileName = `${Date.now()}-${attachmentResult.Name}`
                                let fileContent = attachmentResult.Content

                                //storing the attachment name on database
                                await db('inboxAttachments').insert({
                                    name: fileName,
                                    inboxId: messageWithAttachment.idToAttachment
                                })

                                fs.writeFile(`./uploads/${fileName}`, fileContent, { encoding: 'base64' }, function (err) {
                                    console.log(err);
                                });
                            }
                        }
                    }
                }
            }
        }
        await exchangeWebServiceQueries.markAsRead(ews, folderIdAtributes, 'MarkAllItemsAsRead')
    }
}
