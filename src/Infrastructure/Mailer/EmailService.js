require("dotenv").config();
const nodemailer = require('nodemailer');

module.exports = class EmailService {
    constructor() {
    }

    async emailSender(emails, subject, body, attachments, html) {
        const recipients = this.stringifyEmails(emails);

        let mailTransporter;

        if (process.env?.MAIL_HOST) {
            mailTransporter = nodemailer.createTransport({
                host: process.env.MAIL_HOST,
                secure: false,
                auth: {
                    user: process.env.SENDER_EMAIL,
                    pass: process.env.EMAIL_SENDER_PASSWORD
                },
                tls: {
                    rejectUnauthorized: false
                }
            });
        } else {
            mailTransporter = nodemailer.createTransport({
                service: 'gmail',
                auth: {
                    user: process.env.SENDER_EMAIL,
                    pass: process.env.EMAIL_SENDER_PASSWORD
                },
            });
        }

        let mailDetails = {
            from: `MPDC - Comunications <${process.env.SENDER_EMAIL}>`,
            to: recipients,
            subject,
            //text: body,
            html: this.getMessageBodyTemplate(body),
            attachments
        };

        return await mailTransporter.sendMail(mailDetails)

    }

    stringifyEmails(emails) {
        let recipients = null;

        for (const email of emails) {
            if (recipients === null) {
                recipients = email;
            } else {
                recipients += `,${email}`
            }
        }

        return recipients;
    }

    getMessageBodyTemplate(body) {
        const message = `
            <html>
                <head>
                    <meta http-equiv='Content-Type' content='text/html; charset=utf-8'>
                </head>
                <body>
                    <table style='font-family: 'Segoe UI',Arial, Helvetica, sans-serif; font-size: 12px;' width='100%'>
                        <tr>
                            <th align='left' colspan='2' style='padding: 10px; color: white; background-color: #0d4961; font-weight: normal'>
                                <div style='text-transform: uppercase; font-size: 18px; font-weight: bold; font-family: 'Segoe UI',Arial, Helvetica, sans-serif;'>MPDC Communication Platform</div>
                            </th>
                        </tr>
                        <tr>
                            <td colspan='2' style='font-size: 30px; padding: 20px; vertical-align: top; background-color: #f6f6f6;'>
                                <div style='margin-top: 10px; text-transform: uppercase; font-size: 11px; font-weight: normal; font-family: 'Segoe UI',Arial, Helvetica, sans-serif;'></div>
                            </td>
                        </tr>
                        <tr style='font-size: medium'>
                            <td colspan='2' style='padding: 15px 10px; padding-bottom: 35px; font-size: 16px;'>
                                ${body}
                            </td>
                        </tr>
                        <tr>
                            <td colspan='2' style='border-top: 1px solid #666;'>
                            <br />
                            </td>
                        </tr>
                    </tr>
                    </table>
                    </td>
                    </tr>
            </table>
        </body>
    </html>`

        return message;
    }
}
