require("dotenv").config();

const client = require('twilio')(process.env.TWILIO_ACCOUNT_SID, process.env.TWILIO_AUTH_TOKEN);

module.exports = class TwilioSmsService {
    constructor() {
    }

    async sendSms(contacts, body) {

        const toBinding = contacts.map(number => {
            return JSON.stringify({binding_type: 'sms', address: number});
        });

        const notificationsOptions = {
            toBinding,
            body
        }

        return await client.notify
            .services(process.env.TWILIO_SERVICE_SID)
            .notifications.create(notificationsOptions)
    }
}
