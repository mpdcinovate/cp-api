exports.seed = function (knex) {
    // Deletes ALL existing entries
    return knex('permissions').del()
        .then(function () {
            // Inserts seed entries
            return knex('permissions').insert([
                {code: 'P_VIEW_EMAILS', description: 'can open the inbox and list emails'},
                {code: 'P_SEND_EMAILS', description: 'can send emails through the APP and list sent messages'},
                {code: 'P_CONTACT_CREATE', description: 'can create a new contact'},
                {code: 'P_MANAGE_MESSAGES', description: 'can manage messages'},
                {code: 'P_LIST_MESSAGES', description: 'can list messages'},
                {code: 'P_MANAGE_PUBLICATIONS', description: 'can handle publications'},
                {code: 'P_PUBLISH_CONTENT', description: 'can publish content in home page'},
                {code: 'P_PLATFORM_CONFIGURATION', description: 'add new contacts, users and manage groups'},
                {code: 'P_MANAGE_CONTACTS', description: 'list and add new contacts'},
                {code: 'P_MANAGE_GROUPS', description: 'list and add new groups'},
                {code: 'P_MANAGE_USERS', description: 'list and add new users'},
                {code: 'P_MANAGE_NOTIFICATIONS', description: 'list and answer notifications'},
                {code: 'P_SEND_SMS', description: 'can send messages using sms'},
            ]);
        });
};
