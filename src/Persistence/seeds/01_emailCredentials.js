
exports.seed = function(knex) {
  // Deletes ALL existing entries
  return knex('emailCredentials').del()
    .then(function () {
      // Inserts seed entries
      return knex('emailCredentials').insert([
        {userName: 'communications@portmaputo.com', password: 'Pa$$w0rd', host:'https://smtp.office365.com'},
      ]);
    });
};
