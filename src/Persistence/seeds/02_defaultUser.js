const bcrypt = require("bcrypt-nodejs");

exports.seed = function (knex) {
    // Deletes ALL existing entries
    return knex('users').del()
        .then(async function () {
            const insertedCommunication = await knex('communications')
                .insert({name: 'Soraia', lastName: 'Abdula', email: 'soraia.abdula@portmaputo.com', super: true}).returning('*')

            const hash = bcrypt.hashSync('Pa$$w0rd');

            const insertedUser = insertedCommunication[0];

            //const role = await knex('roles').where({name: 'SuperAdmin'});

            return knex('users').insert([
                {
                    userName: 'soraia.abdula',
                    hash,
                    email: insertedUser.email,
                    communicationId: insertedUser.id,
                    active: true,
                 },
            ]);
        });
};
