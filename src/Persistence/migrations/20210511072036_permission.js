
exports.up = function(knex) {
    return knex.schema.createTable('permissions', table => {
        table.increments('id').primary();
        table.string('code');
        table.string('description');
    })
};

exports.down = function(knex) {
    return knex.schema.dropTable('permissions');
};
