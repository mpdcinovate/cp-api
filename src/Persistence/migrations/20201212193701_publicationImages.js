
exports.up = function(knex) {
    return knex.schema.createTable('publicationImages', table => {
        table.increments('id').primary();
        table.string('name').notNullable();

        table.integer('publicationId')
            .notNullable()
            .references('id')
            .inTable('publications')
            .onUpdate('CASCADE')
            .onDelete('CASCADE');
    })
};

exports.down = function(knex) {
    return knex.schema.dropTable('publicationImages');
};
