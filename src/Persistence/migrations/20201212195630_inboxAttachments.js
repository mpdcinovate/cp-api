exports.up = function (knex) {
    return knex.schema.createTable('inboxAttachments', table => {
        table.increments('id').primary();
        table.string('name').notNullable();

        table.integer('inboxId')
            .notNullable()
            .references('id')
            .inTable('inbox')
            .onUpdate('CASCADE')
            .onDelete('CASCADE');

    })
};

exports.down = function (knex) {
    return knex.schema.dropTable('inboxAttachments')
};
