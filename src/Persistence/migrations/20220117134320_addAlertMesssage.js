
exports.up = function(knex) {
    return knex.schema.table('messages', function (table) {
        table.text('alertText').defaultTo(null);
    })
};

exports.down = function(knex) {
    return knex.schema.table('messages', function (table) {
        table.dropColumn('alertText');
    })
};
