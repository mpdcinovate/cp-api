exports.up = function(knex) {
    return knex.schema.table('messages', function (table) {
        table.timestamp('alertMessage').defaultTo(null);
    })
};

exports.down = function(knex) {
    return knex.schema.table('messages', function (table) {
        table.dropColumn('alertMessage');
    })
};
