exports.up = function (knex) {
    return knex.schema.table('messages', function (table) {
        table.timestamp('scheduledTo').defaultTo(null);
        table.timestamp('actualSendDate').defaultTo(null);
        table.boolean('viaSms').defaultTo(false);
        table.boolean('viaEmail').defaultTo(false);
        table.boolean('alreadySent').defaultTo(false);
    })
};

exports.down = function (knex) {
    return knex.schema.table('messages', function (table) {
        table.dropColumn('scheduledTo');
        table.dropColumn('actualSendDate');
        table.dropColumn('viaSms');
        table.dropColumn('viaEmail');
        table.dropColumn('alreadySent');
    })
};
