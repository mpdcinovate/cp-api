
exports.up = function(knex) {
    return knex.schema.createTable('contacts', table => {
        table.increments('id').primary();
        table.string('name').notNullable();
        table.string('lastName').notNullable();
        table.string('email');
        table.string('phone');
        table.string('gender').notNullable();
        table.string('department');
        table.string('company');
        table.string('designation');
    })
};

exports.down = function(knex) {
  return knex.schema.dropTable('contacts')
};
