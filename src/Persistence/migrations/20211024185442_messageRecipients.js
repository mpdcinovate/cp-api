
exports.up = function(knex) {
    return knex.schema.createTable('messageRecipients', table => {
        table.increments('id').primary();
        table.string('contactEmail');
        table.string('contactPhone');
        table.string('groupName');
        table.integer('messageId')
            .notNullable()
            .references('id')
            .inTable('messages')
            .onUpdate('CASCADE');
    })
};

exports.down = function(knex) {
    return knex.schema.dropTable('messageRecipients')
};
