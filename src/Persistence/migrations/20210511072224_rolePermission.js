exports.up = function (knex) {
    return knex.schema.createTable('rolePermissions', table => {
        table.increments('id').primary();
        table.integer('roleId')
            .notNullable()
            .references('id')
            .inTable('roles')
            .onUpdate('CASCADE');
        table.integer('permissionId')
            .notNullable()
            .references('id')
            .inTable('permissions')
            .onUpdate('CASCADE');
    })
};

exports.down = function (knex) {
    return knex.schema.dropTable('rolePermissions');
};
