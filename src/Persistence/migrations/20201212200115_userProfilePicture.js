
exports.up = function(knex) {
    return knex.schema.createTable('profilePicture', table => {
        table.increments('id').primary();
        table.string('name').notNullable();

        table.integer('userId')
            .notNullable()
            .references('id')
            .inTable('users')
            .onUpdate('CASCADE')
            .onDelete('CASCADE');

    })
};

exports.down = function(knex) {
    return knex.schema.dropTable('profilePicture')
};
