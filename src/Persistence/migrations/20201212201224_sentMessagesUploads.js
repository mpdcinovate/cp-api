exports.up = function(knex) {
    return knex.schema.createTable('sentMessagesUploads', table => {
        table.increments('id').primary();
        table.string('name').notNullable();

        table.integer('sentMessageId')
            .notNullable()
            .references('id')
            .inTable('sentMessages')
            .onUpdate('CASCADE')
            .onDelete('CASCADE');

    })
};

exports.down = function(knex) {
    return knex.schema.dropTable('sentMessagesUploads')
};
