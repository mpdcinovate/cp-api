exports.up = function(knex) {
    return knex.schema.createTable('messages', table => {
        table.increments('id').primary();
        table.text('subject').notNullable();
        table.text('body');
        table.string('sender').notNullable();
        table.timestamp('sent_at').defaultTo(knex.fn.now());
        table.integer('communicationId')
            .notNullable()
            .references('id')
            .inTable('communications')
            .onUpdate('CASCADE');

    })
};

exports.down = function(knex) {
    return knex.schema.dropTable('messages');
};
