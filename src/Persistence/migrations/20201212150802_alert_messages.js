
exports.up = function(knex) {
    return knex.schema.createTable('alertMessages', table => {
        table.increments('id').primary();
        table.string('name').notNullable();
        table.string('body').notNullable();
        table.boolean('default')
    })
};

exports.down = function(knex) {
    return knex.schema.dropTable('alertMessages')
};
