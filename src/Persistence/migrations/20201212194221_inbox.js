
exports.up = function(knex) {
  return knex.schema.createTable('inbox',table => {
      table.increments('id').primary();
      table.string('subject');
      table.string('body');
      table.string('fromName');
      table.string('fromEmail');
      table.boolean('seen');
      table.timestamp('received_at').defaultTo(knex.fn.now());
  })
};

exports.down = function(knex) {
    return knex.schema.dropTable('inbox');
};
