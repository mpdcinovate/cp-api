exports.up = function(knex) {
    return knex.schema.createTable('messageUploads', table => {
        table.increments('id').primary();
        table.string('name').notNullable();

        table.integer('messageId')
            .notNullable()
            .references('id')
            .inTable('messages')
            .onUpdate('CASCADE')
            .onDelete('CASCADE');

    })
};

exports.down = function(knex) {
    return knex.schema.dropTable('messageUploads')
};
