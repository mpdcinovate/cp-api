
exports.up = function(knex) {
    return knex.schema.createTable('groupContacts', table => {
        table.increments('id').primary();
        table.integer('groupId')
            .notNullable()
            .references('id')
            .inTable('groups')
            .onUpdate('CASCADE')
            .onDelete('CASCADE');

        table.integer('contactsId')
            .notNullable()
            .references('id')
            .inTable('contacts')
            .onUpdate('CASCADE')
            .onDelete('CASCADE');
    })
};

exports.down = function(knex) {
      return knex.schema.dropTable('groupContacts');
};
