
exports.up = function(knex) {
    return knex.schema.createTable('users', table => {
        table.increments('id').primary();
        table.string('username').notNullable();
        table.string('email').notNullable();
        table.string('hash').notNullable();
        table.string('recoveryCode');

        table.integer('communicationId')
            .notNullable()
            .references('id')
            .inTable('communications')
            .onUpdate('CASCADE')
            .onDelete('CASCADE');
        
    })
};

exports.down = function(knex) {
    return knex.schema.dropTable('users')
};
