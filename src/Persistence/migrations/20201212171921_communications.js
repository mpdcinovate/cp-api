
exports.up = function(knex) {
    return knex.schema.createTable('communications', table => {
        table.increments('id').primary();
        table.string('name').notNullable();    
        table.string('lastName').notNullable();    
        table.string('email').notNullable();    
        table.string('pictureUrl');
        table.boolean('super').defaultTo(false);   
    })
};

exports.down = function(knex) {
    return knex.schema.dropTable('communications')
};
