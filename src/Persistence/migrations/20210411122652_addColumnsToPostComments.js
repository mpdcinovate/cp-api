
exports.up = function(knex) {
    return knex.schema.table('publicationComments', function (table) {
        table.string('answerText');
        table.string('answeredBy');
        table.timestamp('answeredAt').defaultTo(knex.fn.now());
    })
};

exports.down = function(knex) {
    return knex.schema.table('publicationComments', function (table) {
        table.dropColumn('name');
        table.dropColumn('answerText');
        table.dropColumn('answeredBy');
        table.dropColumn('answeredAt');
    })
};
