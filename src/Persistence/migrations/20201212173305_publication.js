
exports.up = function(knex) {
    return knex.schema.createTable('publications', table => {
        table.increments('id').primary();
        table.string('subject').notNullable();
        table.string('body').notNullable();
        table.timestamp('created_at').defaultTo(knex.fn.now());
        table.integer('communicationsId')
            .notNullable()
            .references('id')
            .inTable('communications')
            .onUpdate('CASCADE');
        
    })
};

exports.down = function(knex) {
    return knex.schema.dropTable('publications')
};
