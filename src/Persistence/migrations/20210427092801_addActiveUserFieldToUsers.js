
exports.up = function(knex) {
    return knex.schema.table('users', function (table) {
        table.boolean('active').defaultTo(false);  
    })
};

exports.down = function(knex) {
    return knex.schema.table('users', function (table) {
        table.dropColumn('active');
    })
};