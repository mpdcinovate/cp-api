
exports.up = function(knex) {
    return knex.schema.createTable('recipients', table => {
        table.increments('id').primary();
        table.integer('sentMessageId')
            .notNullable()
            .references('id')
            .inTable('sentMessages')
            .onUpdate('CASCADE');

        table.integer('contactId')
            .references('id')
            .inTable('contacts')
            .onUpdate('CASCADE');

        table.integer('groupId')
            .references('id')
            .inTable('groups')
            .onUpdate('CASCADE');
        
    })
};

exports.down = function(knex) {
    return knex.schema.dropTable('recipients')
};
