
exports.up = function(knex) {
    return knex.schema.table('recipients', function (table) {
        table.string('contactEmail');
        table.string('contactPhone');
        table.string('groupName');    
    })
};

exports.down = function(knex) {
    return knex.schema.table('recipients', function (table) {
        table.dropColumn('contactEmail');
        table.dropColumn('contactPhone');
        table.dropColumn('groupName');
    })
};

