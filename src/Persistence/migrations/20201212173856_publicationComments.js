
exports.up = function(knex) {
    return knex.schema.createTable('publicationComments', table => {
        table.increments('id').primary();
        table.string('authorName');
        table.string('authorContact'); //email or number
        table.string('commentBody').notNullable();
        table.timestamp('commented_at').defaultTo(knex.fn.now());
        table.boolean('answered').defaultTo(false);
        table.boolean('seen').defaultTo(false);
        table.integer('publicationId')
            .notNullable()
            .references('id')
            .inTable('publications')
            .onUpdate('CASCADE')
            .onDelete('CASCADE');
        
    })
};

exports.down = function(knex) {
    return knex.schema.dropTable('publicationComments');
};
