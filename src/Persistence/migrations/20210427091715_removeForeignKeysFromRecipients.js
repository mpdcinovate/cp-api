
exports.up = function (knex) {
    return knex.schema.table('recipients', function (table) {
        table.dropForeign('contactId');
        table.dropColumn('contactId');
        table.dropForeign('groupId');
        table.dropColumn('groupId');
    })
};

exports.down = function (knex) {
    return knex.schema.table('recipients', function (table) {
        table.integer('contactId')
            .references('id')
            .inTable('contacts')
            .onUpdate('CASCADE');

        table.integer('groupId')
            .references('id')
            .inTable('groups')
            .onUpdate('CASCADE');
    })
};

