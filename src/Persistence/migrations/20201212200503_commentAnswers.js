exports.up = function(knex) {
    return knex.schema.createTable('commentAnswers', table => {
        table.increments('id').primary();
        table.string('authorName').notNullable();
        table.string('authorLastName').notNullable();
        table.string('body').notNullable();
        table.timestamp('answered_at').defaultTo(knex.fn.now());
        table.integer('commentId')
            .notNullable()
            .references('id')
            .inTable('publicationComments')
            .onUpdate('CASCADE')
            .onDelete('CASCADE');
    })
};

exports.down = function(knex) {
    return knex.schema.dropTable('commentAnswers')
};
