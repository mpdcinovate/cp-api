
exports.up = function(knex) {
    return knex.schema.table('users', function (table) {
        table.integer('roleId')
            .references('id')
            .inTable('roles')
            .onUpdate('CASCADE')
    });
}

exports.down = function(knex) {
    return knex.schema.table('users', function (table) {
        table.dropForeign('roleId');
        table.dropColumn('roleId');
    });
}
