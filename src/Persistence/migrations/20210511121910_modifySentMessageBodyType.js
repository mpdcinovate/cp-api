
exports.up = function(knex) {
    return knex.schema.alterTable('sentMessages', function (t) {
        t.text('body').alter();
    });
}

exports.down = function(knex) {
    return knex.schema.alterTable('sentMessages', function (t) {
        t.string('body').alter();
    });
};
