
exports.up = function(knex) {
    return knex.schema.alterTable('inbox', function (t) {
        // drops both not null constraint and the default value
        t.text('body').alter();
    });
}

exports.down = function(knex) {
    return knex.schema.alterTable('inbox', function (t) {
        // drops both not null constraint and the default value
        t.string('body').alter();
    });
};
