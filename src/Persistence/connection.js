const dotenv = require("dotenv");
dotenv.config();

const Knex = require("knex");

const db = Knex({
  client: 'mssql',
  connection: {
    user: 'sa',
    password: process.env.ENV === 'prod' ? process.env.DATABASE_REMOTE_PASSWORD : process.env.DATABASE_PASSWORD,
    port: 1433,
    database: process.env.DATABASE_NAME,
    host: process.env.ENV === 'prod' ? process.env.DATABASE_REMOTE_HOST : process.env.DATABASE_HOST,
    options: {
      encrypt: true,
      enableArithAbort: true
    },
  },
  useNullAsDefault: true,
});

module.exports = {
  db: db
}
