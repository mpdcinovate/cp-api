const fs = require('fs')
//const EWS = require('node-ews')
//InboxMessage CRUD
const InboxMessage = require('./InboxMessage')
//importing exchange server connection info
//const ExchangeConnection = require('./exchangeConnection')
//

module.exports = class ExchangeController {

  //This Function is the "mother" of the class! It commands all microsoft exchange methods that are
  //responsible to retrieve FindItem, GetItem and GetAttachments(each methods has its explanations bellow)

  async buildMessage(req, res, db, ews) {
    const { page, limit } = req.query
    const inboxMessage = new InboxMessage()
    //const conn = new ExchangeConnection();
    //const config = await conn.index(db)

    //initialize node-ews
    //const ews = new EWS(config[0])

    //The getFolder method retrieves properties of @portmaputo Inbox Folder
    const folderProperties = await this.getFolder(ews, 'GetFolder');

    //The GetFolder method returns the number of unread thing (messages in this case!)
    let unreadCount = folderProperties.UnreadCount

    //These attributes are necessary to mark as seen all the messages after return it
    const folderIdAtributes = {
      id: folderProperties.FolderId.attributes.Id,
      changeKey: folderProperties.FolderId.attributes.ChangeKey
    }

    //The findItems method is used to fetch IDs of each messages found on 
    //email inbox so that we can open them
    const messagesId = await this.findItems(ews,'FindItem');
    //const unreadCount = 0
    if (unreadCount !== 0) {
      let stopCondition = 0
      let flag = false
      await messagesId.forEach((msgId) => {
        //Using the ids returned by the FindItem method, getItem Method 
        //returns all specific message property(subject, body, etc.)
        this.getItem(ews, 'GetItem', msgId)
          .then(async result => {
            if (!result.ResponseMessages.GetItemResponseMessage.Items.Message.IsRead) {
              let subject, messageBody, date, fromName, fromEmailAddress;

              //Building the message composition (to save on Database)
              //id = result.ResponseMessages.GetItemResponseMessage.Items.Message.ItemId.attributes.Id
              subject = result.ResponseMessages.GetItemResponseMessage.Items.Message.Subject
              messageBody = result.ResponseMessages.GetItemResponseMessage.Items.Message.Body['$value']
              date = result.ResponseMessages.GetItemResponseMessage.Items.Message.DateTimeCreated
              fromName = result.ResponseMessages.GetItemResponseMessage.Items.Message.From.Mailbox.Name
              fromEmailAddress = result.ResponseMessages.GetItemResponseMessage.Items.Message.From.Mailbox.EmailAddress


              //storing the message in database
              const id = await inboxMessage.createMessage([{
                subject: subject,
                messageBody: messageBody,
                date: date,
                fromName: fromName,
                fromEmailAddress: fromEmailAddress
              }], db)
              stopCondition++

              let attachmentVerification = await this.hasOrNotAttachment(result, id)

              if (attachmentVerification !== undefined) {

                if (attachmentVerification.attachmentProps.AttachmentId) {

                  let attachmentResult = await this.getAttachments(ews, 'GetAttachment', attachmentVerification.attachmentProps.AttachmentId.attributes.Id)
                  let fileName = `${Date.now()}-${attachmentResult.Name}`
                  let fileContent = attachmentResult.Content

                  //storing the attachment name on database
                  await inboxMessage.createAttachment([{
                    attName: fileName,
                    idInboxMessage: attachmentVerification.idToAttachment
                  }], db)

                  fs.writeFile(`./uploads/received_emails/${fileName}`, fileContent, { encoding: 'base64' }, function (err) {
                    console.log(err);
                  });

                }
                //when there are more than one attachment
                else {
                  attachmentVerification.attachmentProps.forEach(async attProp => {
                    let attachmentResult = await this.getAttachments(ews, 'GetAttachment', attProp.AttachmentId.attributes.Id)

                    let fileName = `${Date.now()}-${attachmentResult.Name}`
                    let fileContent = attachmentResult.Content

                    //storing the attachment name on database
                    await inboxMessage.createAttachment([{
                      attName: fileName,
                      idInboxMessage: attachmentVerification.idToAttachment
                    }], db)

                    fs.writeFile(`./uploads/received_emails/${fileName}`, fileContent, { encoding: 'base64' }, function (err) {
                      console.log(err);
                    });

                  })
                }
              }
              if (stopCondition === unreadCount) {
                if (flag === false) {
                  flag = true
                  //querying the database
                  await inboxMessage.index(req, res, db)
                  this.markAsRead(ews, folderIdAtributes, 'MarkAllItemsAsRead')
                }
              }
            }
          })
      })

    }
    else {
      //Here I must respond the client with * from InboxMessage!
      inboxMessage.index(req, res, db)
    }

  }

  async hasOrNotAttachment(result, id) {
    if (result.ResponseMessages.GetItemResponseMessage.Items.Message.HasAttachments) {

      let messageWithAttachment = {
        messageId: result.ResponseMessages.GetItemResponseMessage.Items.Message.ItemId.attributes.Id,
        attachmentProps: result.ResponseMessages.GetItemResponseMessage.Items.Message.Attachments.FileAttachment,
        hasAttachments: true,
        idToAttachment: id
      }
      return messageWithAttachment
    }
  }

  async getFolder(ews, ewsFunction) {

    const ewsArgs = {
      'FolderShape': {
        'BaseShape': 'Default'
      },
      'FolderIds': {
        'DistinguishedFolderId': {
          'attributes': {
            'Id': 'inbox'
          }
        }
      }
    }

    const response = await ews.run(ewsFunction, ewsArgs)
    return response.ResponseMessages.GetFolderResponseMessage.Folders.Folder
  }

  //This functions, based in the ewsArgs(query) retrieves 
  //all the Messages(Items) Ids in an array
  async findItems(ews, ewsFunction) {
    const ewsArgs = {
      'attributes': {
        'Traversal': 'Shallow'
      },
      'ItemShape': {
        'BaseShape': 'IdOnly'
      },
      'ParentFolderIds': {
        'DistinguishedFolderId': {
          'attributes': {
            'Id': 'inbox'
          }
        }
      }

    }

    const response = await ews.run(ewsFunction, ewsArgs)

    return response.ResponseMessages.FindItemResponseMessage.RootFolder.Items.Message
  }

  //Based on the Ids returned in the findItems() it fetches all inbox messages content(subject, body, etc)
  async getItem(ews, ewsFunction, messagesId) {
    let ewsArgs, k = [];

    let id = messagesId.ItemId.attributes.Id;
    let changeKey = messagesId.ItemId.attributes.ChangeKey;

    ewsArgs = {
      'ItemShape': {
        'BaseShape': 'Default',
        'IncludeMimeContent': 'false',
        'BodyType': 'Text',
        'AdditionalProperties': {
          'FieldURI': {
            'attributes': {
              'FieldURI': 'item:Attachments'
            }
          }
        }
      },

      'ItemIds': {
        'ItemId': {
          'attributes': {
            'Id': id,
            'ChangeKey': changeKey
          }
        }
      }
    }

    const response = await ews.run(ewsFunction, ewsArgs)

    return response
  }

  async getAttachments(ews, ewsFunction, messageId) {
    let ewsArgs;

    ewsArgs = {
      'AttachmentShape': {
        'BodyType': 'HTML',
      },
      'AttachmentIds': {
        'AttachmentId': {
          'attributes': {
            'Id': messageId
          }
        }
      }
    }

    const response = await ews.run(ewsFunction, ewsArgs)

    return response.ResponseMessages.GetAttachmentResponseMessage.Attachments.FileAttachment
  }

  async markAsRead(ews, folderIdAtributes, ewsFunction) {

    const ewsArgs = {
      'ReadFlag': true,
      'SuppressReadReceipts': true,
      'FolderIds': {
        'FolderId': {
          'attributes': {
            'Id': folderIdAtributes.id,
            'ChangeKey': folderIdAtributes.changeKey
          }
        }

      }
    }

    await ews.run(ewsFunction, ewsArgs)
  }
}
