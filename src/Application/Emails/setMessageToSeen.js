const setMessageToSeen = (req, res, db) => {
    const { id } = req.params

        db('inbox')
            .where({ id: id })
            .update({
                seen: true
            })
            .then(response => res.json(response))
            .catch((err) => res.json(err))
}

module.exports = setMessageToSeen;