const getEmailsGroupByCompany = async (req, res, db) => {
    const contacts = await db('contacts')
        .whereNot({company: ''})
        .orWhereNot({company: null})
        .select('company', 'email')

    const companiesList = contacts?.map(contact => ({company: contact.company}))

    const emails = contacts?.map(contact => contact.email)

    let companiesAndTheirsEmails = []

    let index = 0
    for (const company of companiesList) {
        const messages = await db('inbox').where('fromEmail', emails[index])

        if(messages?.length > 0){
            companiesAndTheirsEmails.push({company,emails:messages, totalUnread: messages.filter(message => !message.seen)?.length});
        }
        index++;
    }
    
    return res.status(200).json(companiesAndTheirsEmails)



    //const 
    // const companiesWithEmailsOnInbox = await db('contacts')
    //     .join('InboxMessage', 'contacts.email', '=', 'InboxMessage.fromEmail')
    //     .select('contacts.id', 'contacts.company')

    // if (companiesWithEmailsOnInbox.length !== 0) {
    //     const companiesWithoutDuplicate = Array.from(new Set(companiesWithEmailsOnInbox.map(aux => aux.id)))
    //         .map(id => {
    //             return companiesWithEmailsOnInbox.find(aux => aux.id === id)
    //         })

    //     let auxcompaniesWithoutDuplicate = companiesWithoutDuplicate
    //     let index = 0

    //     for (let contact of companiesWithoutDuplicate) {
    //         const emailsFromEachCompany = await db('contacts')
    //             .join('Extern_Contacts', 'contacts.id', '=', 'Extern_Contacts.idContacts')
    //             .where('Extern_Contacts.company', '=', contact.company)
    //             .select('contacts.email')

    //         const emailsWithoutAttribute = emailsFromEachCompany.map(email => email.email)

    //         const messagesPerCompany = await db('InboxMessage')
    //             .whereIn('fromEmailAddress', emailsWithoutAttribute)

    //         const auxcompaniesWithoutDuplicate2 = auxcompaniesWithoutDuplicate.map((aux, j) => {
    //             if (j === index) {
    //                 return { ...aux, emails: messagesPerCompany }
    //             }
    //             return aux
    //         })

    //         auxcompaniesWithoutDuplicate = auxcompaniesWithoutDuplicate2
    //         index++
    //     }

    //     const companiesWithTotalUnread = auxcompaniesWithoutDuplicate.map(company => {
    //         const totalUnread = company.emails.filter(email => email.seen === 0)
    //         return { ...company, totalUnread: totalUnread.length }
    //     })

    //     return res.json(companiesWithTotalUnread)
    // }
    // else {
    //     return res.json([])
    // }

}

module.exports = getEmailsGroupByCompany