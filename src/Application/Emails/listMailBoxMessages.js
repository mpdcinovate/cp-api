const ExchangeWebServiceMessageBuilder = require('../../Infrastructure/ExchangeMailService/ExchangeWebServiceMessageBuilder')

module.exports = async function listMailBoxMessages(req, res, db,ews) {
    const ewsMessageBuilder = new ExchangeWebServiceMessageBuilder();

    await ewsMessageBuilder.buildMessage(req, res, db, ews)

    const total = await db('inbox').select('id');

    const unread = await db('inbox').select('id').where('seen', '=', false)

    const messages = await db.select('*')
        .from('inbox')
        .orderBy('received_at', 'desc');
        //.limit(limit)
        //.offset((page - 1) * limit)

    return res.status(200).json({
        'total': total.length,
        //'totalPages': totalPages,
        'unread': unread.length,
        'messages': messages
    })
}
