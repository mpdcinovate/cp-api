
module.exports = class InboxMessage {

    createMessage = async (content, db) => {

        const id = await db.insert(content).returning('ID')
            .into('InboxMessage').then((id) => {
                //console.log(content)
                return id[0]
            })

        return (id)
    }

    createAttachment = async (content, db) => {
        //console.log(content)
        await db.insert(content)
            .into('mailAttachment')
    }

    update = async (req, res, db) => {
        const { id } = req.params

        db('InboxMessage')
            .where({ id: id })
            .update({
                seen: false
            })
            .then(response => res.json(response))
            .catch((err) => res.json(err))

    }

    index = async (req, res, db) => {
        const { page, limit } = req.query;
        const total = await db('inbox').select('id');
        const unread = await db('inbox').select('id').where('seen', '=', false)

        //const totalPages = Math.ceil((total.length / limit))

        return db.select('*').from('inbox')
            .orderBy('received_at', 'desc')
            //.limit(limit)
            //.offset((page - 1) * limit)
            .then(messages => {
                res.json({
                    'total': total.length,
                    //'totalPages': totalPages, 
                    'unread': unread.length,
                    'messages': messages
                })
            })
            .catch(err => res.json(err))
    }

    show = async (req, res, db) => {
        const { id } = req.params

        const messageWithAtt = await db('InboxMessage')
            .join('mailAttachment', 'InboxMessage.ID', '=', 'mailAttachment.idInboxMessage')
            .where('mailAttachment.idInboxMessage', id)
            .select('InboxMessage.ID', 'InboxMessage.subject', 'InboxMessage.messageBody', 'InboxMessage.date', 'InboxMessage.fromName', 'InboxMessage.fromEmailAddress', 'mailAttachment.attName')

        if (messageWithAtt.length > 0) {
            return res.json(messageWithAtt)
        }
        else {
            db.select('ID', 'subject', 'messageBody', 'date', 'fromName', 'fromEmailAddress')
                .from('InboxMessage')
                .where('id', '=', id)
                .then(data => res.json(data))
                .catch(() => res.status(400).json('something is bad!'));
        }

    }

    delete = async (req, res, db) => {
        const { id } = req.params;

        db('InboxMessage').where('ID', id).del()
            .then(() => res.json(id))
            .catch(err => {
                res.status(400).json(err)
            })
    }

    categorized = async (req, res, db) => {

        

    }

}