const getInboxMessageById = async (req, res, db) => {
    const { id } = req.params

    let attachments = []

    const message = await db('inbox')
        .where({ id: id })

    if (message?.length > 0) {
        attachments = await db('inboxAttachments')
            .where({ inboxId: message[0].id })
    }
    else{
        return res.status(404).json({error: "Not Found"})
    }

    // await db('inbox')
    //     .where({id: id})
    //     .update({seen: false})

    return res.json({...message[0], attachments: attachments})

}

module.exports = getInboxMessageById;