const EmailService = require('../../Infrastructure/Mailer/EmailService')
const emailService = new EmailService();
const TwilioSms = require('../../Infrastructure/TwilioSms/TwilioSmsService')
const smsService = new TwilioSms();
const getDateNow = require('../../Utils/getDateNow')

module.exports = async function sendMessage(req, res, db) {
    const {subject, body, contactsIds, groupsIds, sms, mail, alertText} = JSON.parse(req.body.content);
    let attachments = [], contactsIdsToSendMessage;

    const trx = await db.transaction();

    try {
        let message = await trx('messages').insert({
            subject,
            body,
            communicationId: req.user.id,
            sender: req.user.email,
            viaSms: sms,
            viaEmail: mail,
            alreadySent: true,
            alertText,
            sent_at: getDateNow()
        })
            .returning('*');

        if (!message?.length) {
            await trx.rollback();
            return res.status(500).json({error: `Failed to save Message`})
        }

        message = message[0]

        if (req?.files.length > 0) {
            const messageAttachmentData = req?.files?.map(file => {
                attachments.push({
                    filename: file.filename.slice(14, file.filename.length),
                    path: `./uploads/${file.filename}`
                })
                return {name: file.filename, messageId: message.id}
            })

            message = await trx('messageUploads')
                .insert(messageAttachmentData)
                .returning('*')
                .then(insertedUploads => {
                    return ({...message, attachments: insertedUploads})
                });

            if (!message) {
                await trx.rollback();
                return res.status(500).json({error: `Failed to save Message`})
            }
        }

        if (contactsIds?.length > 0) {
            const contacts = await trx('contacts').whereIn('id', contactsIds)

            const contactsRecipients = contacts.map(contact => (
                {messageId: message.id, contactEmail: contact.email, contactPhone: contact.phone}
            ))

            const messageRecipients = await trx('messageRecipients')
                .insert(contactsRecipients)
                .returning('*');

            if (!messageRecipients?.length) {
                await trx?.rollback();
                return res.status(500).json({
                    error: `Failed to store group recipient with error`
                })
            }
        }

        if (groupsIds.length > 0) {

            const groups = await trx('groups').whereIn('id', groupsIds)

            const groupRecipients = groups.map(group => (
                {messageId: message.id, groupName: group.name}
            ))

            const messageRecipients = await trx('messageRecipients')
                .insert(groupRecipients)
                .returning('*');

            if (!messageRecipients?.length) {
                await trx?.rollback();
                return res.status(500).json({error: `Failed to store group recipient with error`})
            }
        }


        const contactsInGroups = await trx('groupContacts')
            .select('contactsId')
            .whereIn('groupId', groupsIds)

        /*  if (!contactsInGroups?.length) {
              await trx?.rollback();
              return res.status(500).json({error: `Failed to get Groups`})
          }*/

        const groupContactsIds = contactsInGroups?.map(contactId => contactId.contactsId)

        const concatenatedContactsIds = groupContactsIds?.concat(contactsIds);
        contactsIdsToSendMessage = [...new Set(concatenatedContactsIds)];


        const contacts = await trx('contacts')
            .select('email', 'phone')
            .whereIn('id', contactsIdsToSendMessage)

        if (!contacts?.length) {
            await trx?.rollback();
            return res.status(500).json({error: `Failed to get Groups`})
        }

        if (mail) {
            const contactsWithEmails = contacts?.filter(contact => contact.email?.length);
            const emails = contactsWithEmails.map(contact => contact.email);

            if (emails.length > 0) {
                const resp = await emailService.emailSender(
                    emails,
                    subject,
                    body,
                    attachments,
                    "",
                    "");
            }
        }

        if (sms) {
            const contactsWithPhoneNumbers = contacts?.filter(contact => contact.phone?.length);
            const phoneNumbers = contactsWithPhoneNumbers.map(contact => contact.phone);

            if (phoneNumbers.length > 0) {
                const resp = await smsService.sendSms(phoneNumbers, body)
                console.log(resp)
            }
        }


        if (alertText !== '') {
            const contactsWithPhoneNumbers = contacts?.filter(contact => contact.phone?.length);
            const phoneNumbers = contactsWithPhoneNumbers.map(contact => contact.phone);
            if (phoneNumbers.length > 0) {
                const resp = await smsService.sendSms(phoneNumbers, alertText)
                console.log(resp)
            }
        }

        await trx?.commit();
        return res.json(message)

    } catch (e) {
        console.log(e)
        await trx?.rollback();
        return res.status(500).json({error: `Failed with error ${e?.message}`})
    }
}
