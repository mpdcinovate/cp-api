const getDateNow = require('../../Utils/getDateNow');

module.exports = async function scheduleMessage(req, res, db) {
    const {subject, body, contactsIds, groupsIds, sms, mail, alertText, scheduledTo} = JSON.parse(req.body.content);
    let attachments = [];
    const trx = await db.transaction();

    try {
        let date = new Date(scheduledTo);
        date.setHours(date.getHours() + 2);

        let message = await trx('messages').insert({
            subject,
            body,
            communicationId: req.user.id,
            sender: req.user.email,
            scheduledTo: date,
            viaSms: sms,
            viaEmail: mail,
            alreadySent: false,
            alertText,
            sent_at: null
        })
            .returning('*');

        if (!message?.length) {
            await trx.rollback();
            return res.status(500).json({error: `Failed to save Message`})
        }

        message = message[0]

        if (req?.files.length > 0) {
            const messageAttachmentData = req?.files?.map(file => {
                attachments.push({
                    filename: file.filename.slice(14, file.filename.length),
                    path: `./uploads/${file.filename}`
                })
                return {name: file.filename, messageId: message.id}
            })

            message = await trx('messageUploads')
                .insert(messageAttachmentData)
                .returning('*')
                .then(insertedUploads => {
                    return ({...message, attachments: insertedUploads})
                });

            if (!message) {
                await trx.rollback();
                return res.status(500).json({error: `Failed to save Message`})
            }
        }

        if (contactsIds?.length > 0) {
            const contacts = await trx('contacts').whereIn('id', contactsIds)

            const contactsRecipients = contacts.map(contact => (
                {messageId: message.id, contactEmail: contact.email, contactPhone: contact.phone}
            ))

            const messageRecipients = await trx('messageRecipients')
                .insert(contactsRecipients)
                .returning('*');

            if (!messageRecipients?.length) {
                await trx?.rollback();
                return res.status(500).json({
                    error: `Failed to store group recipient with error`
                })
            }
        }

        if (groupsIds.length > 0) {

            const groups = await trx('groups').whereIn('id', groupsIds)

            const groupRecipients = groups.map(group => (
                {messageId: message.id, groupName: group.name}
            ))

            const messageRecipients = await trx('messageRecipients')
                .insert(groupRecipients)
                .returning('*');

            if (!messageRecipients?.length) {
                await trx?.rollback();
                return res.status(500).json({error: `Failed to store group recipient with error`})
            }
        }

        await trx?.commit();
        return res.json(message)

    } catch (e) {
        console.log(e)
        await trx?.rollback();
        return res.status(500).json({error: `Failed with error ${e?.message}`})
    }

}
