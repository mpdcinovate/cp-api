module.exports = async function listMessages (req, res, db) {

    const { page, limit } = req.query;
    const total = await db('messages').select('id');

    const messages = await db('messages')
        .select('*')
        .orderBy('id', 'desc')
        .limit(limit)
        .offset((page- 1) * limit)
        //.catch(err => res.status(400).json(`failed to fetch messages with error ${err}`))

    const ids = messages.map(message => message.id)

    const messageUploads = await db('messageUploads')
        .whereIn('messageId', ids)
        .select('name', 'messageId')

    const auxMessages = messages.map(message => {
        let attachments = []
        for (const messageUpload of messageUploads) {
            if (messageUpload?.messageId === message.id) {
                attachments.push(messageUpload.name)
            }
        }

        return {...message, attachments}
    })

    let messagesToReturn = [];
    for (const auxMessage of auxMessages) {
        const recipientsContacts = await db('messageRecipients')
            .where('messageId', auxMessage.id)
            .select('contactEmail', 'contactPhone','groupName')

        const groupNames = recipientsContacts.map(recipientsContact => recipientsContact.groupName)

        const groupIds = await db('groups')
            .whereIn('name', groupNames)
            .select('id');

        const groupContacts = await db('groupContacts')
            .whereIn('groupId', groupIds.map(groupId => groupId.id))
            .select('contactsId')


        const auxRecipientsContactsEmails = recipientsContacts.map(recipientsContact => recipientsContact.contactEmail)
        const auxRecipientsContactsPhones = recipientsContacts.map(recipientsContact => recipientsContact.contactPhone)
        const auxContactsIds = groupContacts.map(groupContact => groupContact.contactsId)

        const contacts = await db('contacts')
            .whereIn('email', auxRecipientsContactsEmails)
            .orWhereIn('phone', auxRecipientsContactsPhones)
            .orWhereIn('id', auxContactsIds)
            .select('id', 'name', 'lastName')

        const contactsFullNames = contacts.map(contact => ({
            id: contact.id,
            fullName: `${contact.name} ${contact.lastName}`
        }))

        messagesToReturn.push({...auxMessage, contactsFullNames})
    }

    res.json({'count': total.length, 'messages': messagesToReturn})
}
