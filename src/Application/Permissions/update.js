module.exports = async function update(request, response, db){
    const {id} = request.params;
    const {code, description} = request.body;

    let permission = await db('permissions')
        .where({id});

    if(permission?.length === 0){
        return response.status(404).json({error: `Permission with Id ${id} not found`});
    }

    permission = await db('permissions').where({id})
        .update({code, description})
        .returning('*');

    if(permission?.length > 0){
        return response.status(200).json(permission[0]);
    }

    return response.status(400).json({error: 'Failed to update Permission'})
}
