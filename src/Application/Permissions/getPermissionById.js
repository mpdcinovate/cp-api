module.exports = async function getById(request, response, db){
    const {id} = request.params;

    const permission = await db('permissions')
        .where({id});

    if(permission?.length === 0){
        return response.status(404).json({error: `Permission with Id ${id} not found`});
    }

    return response.status(200).json(permission[0]);
}
