module.exports = async function list(request, response, db){
    return response
        .status(200)
        .json(await db('permissions').select('*'));
}
