module.exports = async function remove(request, response, db) {
    const {id} = request.params;

    let permission = await db('permissions')
        .where({id});

    if (permission?.length === 0) {
        return response.status(404).json({error: `Permission with Id ${id} not found`});
    }

    permission = await db('permissions').where({id})
        .del()
        .returning('*');

    if (permission?.length > 0) {
        return response.status(200).json(permission[0]);
    }

    response.status(500).json({error: `Failed to remove the permission with Id ${id}`});
}
