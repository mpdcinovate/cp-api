const create = require('./createPermission');
const getById = require('./getPermissionById');
const list = require('./listPermissions');
const remove = require('./removePermission');
const update = require('./update');

module.exports = {
    create,
    getById,
    list,
    remove,
    update
}
