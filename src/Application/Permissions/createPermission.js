module.exports = async function create(request, response, db) {
    const {code, description} = request.body;

    const permission = await db('permissions')
        .insert({code, description})
        .returning('*');

    if (permission?.length > 0) {
        return response.status(200).json(permission[0]);
    }

    return response.status(500).json(`Failed to create permission ${code}`);
}
