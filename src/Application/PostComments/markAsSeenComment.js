module.exports = function markAsSeenComment(req, res, db) {
    const { id } = req.params;

    db('publicationComments')
        .where('seen', false)
        .update('seen', true)
        .returning('*')
        .then(data => res.json(data))
        .catch(err => {
            res.status(400).json({ error: 'Failed to mark as seen' })
        })
}
