module.exports = class Comentarios{



    update = async (req, res, db) => {
        
        db('Comentarios')
        .update('seen', 'true')
        .then(data => res.json(data))
        .catch(err => {
            res.status(400).json(err)})
    }

    deleteComment = async (req, res, db) => {
        const {idComentario} = req.body;

        db('Comentarios').where('idComentario',idComentario).del()
        .then(data => res.json(data))
        .catch(err => {
            res.status(400).json(err)})
    }

}
