const SendEmail = require('../../Infrastructure/Mailer/EmailService')
const SendSMS = require('../../Infrastructure/TwilioSms/TwilioSmsService')
const getDateNow = require('../../Utils/getDateNow');

module.exports = async function replyToComment(req, res, db) {
    const {commentId, answerText} = req.body;
    const loggedUser = req.user

    const verifyUser = await db('communications')
        .where('email', loggedUser.email)

    if (verifyUser?.length === 0) {
        return res.status(401).json({error: 'You are not allowed to reply a comment'})
    }

    let comment = await db('publicationComments')
        .where('id', commentId)
        .returning('*')
        .catch(err => {
            return res.status(400).json({error: err})
        })

    if (comment?.length === 0) {
        return res.status(404).json({error: 'Comment not found'})
    }

    comment = await db('publicationComments').update({
        answerText,
        answered: true,
        answeredBy: loggedUser.email,
        answeredAt: getDateNow()
    })
        .where('id', commentId)
        .returning('*')
        .catch(err => {
            return res.status(400).json({error: `Failed to answer comment with error: ${err}`})
        })

    const flag = comment[0].authorContact.indexOf('@')

    if (flag === -1) {
        const SmsSender = new SendSMS();
        await SmsSender.sendSms([comment[0].authorContact], answerText)
    } else {
        const EmailSender = new SendEmail();
        await EmailSender.emailSender(
            [comment[0].authorContact],
            'Response about you Comment',
            answerText,
            null,
            '',
            '')
    }

    return res.json(comment[0])


}
