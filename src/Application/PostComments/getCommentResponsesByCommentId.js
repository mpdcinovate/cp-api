module.exports = async function getCommentResponsesByCommentId(req, res, db) {
    const {id} = req.params;

    const commentResponse = await db('publicationComments')
        .where('id', id)
        .orderBy('id', 'desc')
        .catch(err => {
            return res.status(400).json({error: `Failed to retrieve Answers with error: ${err}`})
        })

    return res.json(commentResponse[0]);
}
