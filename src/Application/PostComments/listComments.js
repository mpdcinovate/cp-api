module.exports = function listComments(res, db) {
    db('publicationComments')
        .orderBy('id', 'desc')
        .then(post => res.json(post))
        .catch(err => {
            res.status(400).json({error: `failed to fetch comments with error: ${err}`})})
}
