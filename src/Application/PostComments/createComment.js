const getDateNow = require('../../Utils/getDateNow');
module.exports = function createComment(req, res, db) {
    const {authorContact, authorName, commentBody, publicationId} = req.body;

    db('publicationComments').insert({
        authorContact,
        authorName,
        commentBody,
        publicationId,
        commented_at: getDateNow(),
    })
        .returning('*')
        .then(result => res.json(result[0])
        )
        .catch(err => res.status(400).json({error: `Failed to create comment with error: ${err}`}))
}
