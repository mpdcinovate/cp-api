module.exports = async function listIndexedComments(req, res, db) {
    const { page, limit } = req.query;
    const total = await db('publicationComments').select('id');

    const publicationComments = await db('publicationComments')
        .orderBy('id', 'desc')
        .limit(limit)
        .offset((page - 1) * limit)
        // .then(post => {
        //     return res.json({'count': total.length, 'data': post})
        // })
        .catch(err => res.status(400).json({ error: "failed to fetch comments" }))

    let auxComments = [];

    for (const comment of publicationComments) {
        const publication = await db('publications')
            .where('id', comment.publicationId)
            .select('subject', 'body');

        auxComments.push({ ...comment, publicationSubject: publication[0].subject, publicationBody: publication[0].body });
    }

    return res.json({'count': total.length, 'data': auxComments})


}
