module.exports = async function removeRole(req, res, db) {
    const {id} = req.params;

    let role = await db('roles')
        .where({id})


    if (role?.length === 0) {
        return res.status(404).json({error: `role with Id ${id} not found!`});
    }

    role = await db('roles').where({id})
        .del()
        .returning('*');

    if (role?.length > 0) {
        return res.json(role[0]);
    }

    res.status(500).json({error: `Failed to remove Role with Id ${id}`});
}
