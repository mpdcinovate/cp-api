module.exports = async function (req, res, db) {
    const {id} = req.params;
    const {name} = req.body;

    let role = await db('roles')
        .where({id})

    if (role?.length === 0) {
        return res.status(404).json({error: `role with Id ${id} not found!`});
    }

    role = await db('roles').where({id})
        .update({name})
        .returning('*');

    if (role?.length > 0) {
        return res.status(200).json(role[0]);
    }

    return res.status(400).json({error: `Failed to update role`})

}
