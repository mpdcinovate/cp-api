module.exports = async function createRole(req, res, db) {
    const {name} = req.body;

    const role = await db('roles').insert({name}).returning('*');

    if (role?.length > 0) {
        return res.status(200).json(role[0]);
    }
    return res.status(500).json(`Failed to create role ${name}`);
}
