const create = require('./createRole');
const getById = require('./getRoleById');
const list = require('./listRoles');
const remove = require('./removeRole');
const update = require('./updateRole');

module.exports = {
    create,
    getById,
    list,
    remove,
    update
}
