module.exports = async function getById(req, res, db) {
    const {id} = req.params;

    const role = await db('roles')
        .where({id});

    if(role?.length === 0){
        return res.status(404).json({error: `Role with id ${id} not found`});
    }

    return res.status(200).json(role[0]);
}
