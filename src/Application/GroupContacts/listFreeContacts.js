module.exports = async function listFreeContacts(req, res, db) {
    const {groupId} = req.params;

    const groupContacts = await db('groupContacts')
        .where('groupId', groupId)
        .select('contactsId')
        .catch(err => {
            return res.status(400).json({error: err})
        })

    const contactIds = groupContacts?.map(contact => contact.contactsId)

    return res.json(await db('contacts').whereNotIn('id', contactIds).returning('*'))
    
}