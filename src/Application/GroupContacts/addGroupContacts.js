const addGroupContacts = async (req, res, db) => {
    const {groupId, contactsIds} = req.body;


    try {
        for (const item of contactsIds) {
            const contacts = await db('contacts').where('id', item)

            if (contacts?.length === 0) {
                return res.status(404).json({error: `contact ${item} does not exists`});
            }

            const groupContact = await db('groupContacts')
                .where('contactsId', item)
                .andWhere('groupId', groupId)
                .returning('*')

            if (groupContact?.length > 0) {
                return res.status(400).json({error: `contact with id ${item} already registered at group ${groupId}`})
            }
        }

        const membersToAdd = contactsIds?.map(contactId => ({groupId: groupId, contactsId: contactId}))

        return res.json(await db('groupContacts').insert(membersToAdd).returning("*"));

    } catch (e) {
        return res.status(400).json({error: `An error occurred with. details ${e}`})
    }
}

module.exports = addGroupContacts;
