module.exports = async function listGroupContactsByGroupId(req, res, db) {
    const {groupId} = req.params;

    const groupContacts = await db('groupContacts')
        .where('groupId', groupId)
        .select('contactsId')
        .catch(err => {
            return res.status(400).json({error: err})
        })

        console.log(groupContacts)

    if(groupContacts?.length === 0){
        return res.status(404).json({error: `Group Not Found`})
    }

    const contactIds = groupContacts.map(contact => contact.contactsId)

    return res.json(await db('contacts').whereIn('id', contactIds))
    
}