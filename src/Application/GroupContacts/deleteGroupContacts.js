const deleteGroupContacts = async (req, res, db) => {
    const {contactId, groupId} = req.body;

    db('groupContacts')
        .where('contactsId', contactId)
        .andWhere('groupId', groupId)
        .del().returning('*')
        .then(result => res.json(result))
        .catch(err => {
            res.status(400).json(err)
        })
}

module.exports = deleteGroupContacts;
