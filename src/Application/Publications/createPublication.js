const getDateNow = require('../../Utils/getDateNow')
module.exports = async function (req, res, db) {
    const { subject, body, communicationsId } = JSON.parse(req.body.content);

    const transaction = await db.transaction();

    transaction('publications').insert({
        subject,
        body,
        communicationsId,
        created_at: getDateNow()
    })
        .returning('*')
        .then(async insertedPublication => {
            if (req?.files.length > 0) {
                const publicationImagesData = req?.files?.map(file => (
                    { name: file.filename, publicationId: insertedPublication[0].id }
                ))

                transaction('publicationImages')
                    .insert(publicationImagesData)
                    .returning('*')
                    .then(async insertedAttatchments => {
                        await transaction.commit()
                        return res.json({ ...insertedPublication[0], attatchments: insertedAttatchments })
                    })
                    .catch(async err => {
                        await transaction.rollback();
                        return res.status(400).json({error: `Failed to save publications`})
                    })
            }
            else {
                await transaction.commit();
                return res.json({ ...insertedPublication[0], attatchments: [] })
            }
        })
        .catch(async err => {
            await transaction.rollback();
            return res.status(400).json({error: `Failed to save publications`})
        })
}
