module.exports = async function (req, res, db) {
    const { page, limit } = req.query;
    const total = await db('publications').select('id');

    const publications = await db('publications')
        .select('id', 'subject', 'body', 'communicationsId', 'created_at')
        .orderBy('id', 'desc')
        .limit(limit)
        .offset((page- 1) * limit)

    const ids = publications.map(pub => pub.id)

    const publicationImages = await db('publicationImages')
        .whereIn('publicationId', ids)
        .select('name', 'publicationId')

    const auxPublication = publications.map(pub => {
        let attachments = []
        for (const publicationImage of publicationImages) {
            if(publicationImage?.publicationId === pub.id){
                attachments.push(publicationImage.name)
            }
        }

        return {...pub, attachments}
    })

    res.json({'count':total.length,'posts':auxPublication})
}
