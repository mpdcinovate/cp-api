module.exports = async function getPublicationById(req, res, db) {
    const {id} = req.params;

    const publication = await db('publications')
        .where('id', id);

    const publicationImages = await db('publicationImages')
        .where('publicationId', id)

    if (publication.length > 0) {
        return res.json({...publication, attachments: publicationImages});
    }
    else {
        return res.json({});
    }
}
