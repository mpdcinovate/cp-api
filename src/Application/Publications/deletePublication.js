module.exports = function deletePublication (req, res, db) {
    const { id } = req.params;

    db('publications').where({id})
        .del()
        .returning('*')
        .then(data => res.json(data[0]))
        .catch(err => {
            console.log(err)
            return res.status(400).json(err)
        })
}
