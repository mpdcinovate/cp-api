const getUsers = async (res, db) => {
    await db('communications').select('*').then(data => {
        res.status(200).json(data)
    })
        .catch(err => res.status(400).json('something is bad!'))
}

module.exports = getUsers;
