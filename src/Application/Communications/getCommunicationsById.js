const getCommunicationsById = async (req, res, db) => {
    const { id } = req.params;

    const communicationsUser = await db.select('*').from('communications')
        .where('id', '=', id)
        .catch(err => res.status(400).json('something is bad!'));

    const user = await db('users')
        .where('communicationId', id)
        .select('username')

    return res.json({ ...communicationsUser[0], userName: user[0].username })
}

module.exports = getCommunicationsById;

