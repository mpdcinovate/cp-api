const updateCommunicationUser = async (req, res, db) => {
    const {name, lastName, email, userName} = req.body;
    const {id} = req.params;

    const communicationUser = await db.select('*').from("communications")
        .where('id', '=', id)
        .catch(err => res.status(400).json({error: err}));

    if (communicationUser?.length === 0) {
        return res.status(404).json({error: "User Not Found!"})
    }

    const transaction = await db.transaction();

    const resultUpdateCommunication = await transaction('communications')
        .where({id: id})
        .update({name, lastName, email})
        .returning("*")
        .catch(err => {
            transaction?.rollback();
            return res.status(400).json({error: err})
        });

    // const index = email.indexOf("@")

    // const userName = email.slice(0, index);

    await transaction("users")
        .where({communicationId: id})
        .update({email, username: userName.toLocaleLowerCase()})
        .catch(err => {
            transaction?.rollback();
            return res.status(400).json({error: err})
        });

    transaction?.commit();

    return res.status(200).json(resultUpdateCommunication);
}

module.exports = updateCommunicationUser;
