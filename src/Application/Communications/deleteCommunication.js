const deleteCommunication = async (req, res, db) => {
    const {id} = req.params;

    await db('communications').where('id', '=', id).del().returning("*")
        .then(response => res.json(response))
        .catch((err) => res.status(400).json({error: `Failed to delete user. Error: ${err}`}))
}

module.exports = deleteCommunication;

