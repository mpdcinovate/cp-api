const setSuperUser = async (req, res, db) => {

    const {communicationId} = req.params

    const communicationUser = await db('users')
        .where({communicationId})

    if(communicationUser?.length === 0){
        return res.status(404).json({error: `User Not Found`})
    }

    if(communicationUser[0].super){
        return res.status(400).json({error: `User is already Super`})
    }

    db('communications').where({id: communicationId}).update({super: true})
        .returning('*')
        .then(response => res.json({...response[0], hash: ''}))
        .catch(err => res.status(400).json({error: `Failed to set user to super...${err}`
}))
}

module.exports = setSuperUser;
