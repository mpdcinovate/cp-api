const unsetSuperUser = async (req, res, db) => {

    const {communicationId} = req.params

    const communicationUser = await db('communications')
        .where({id: communicationId})

    if(communicationUser?.length === 0){
        return res.status(404).json({error: `User Not Found`})
    }

    if(!communicationUser[0].super){
        return res.status(400).json({error: `User is not a super user`})
    }

    db('communications').where({id:communicationId}).update({super: false})
        .returning('*')
        .then(response => res.json({...response[0], hash: ''}))
        .catch(err => res.status(400).json({error: `Failed to unset user from super...${err}`
        }))
}

module.exports = unsetSuperUser;
