const bcrypt = require("bcrypt-nodejs");
const SendEmail = require('../../Infrastructure/Mailer/EmailService')

const createUser = async (req, res, db) => {
    let {name, lastName, email, roleId} = req.body;

    const trx = await db.transaction();

    const role = await db('roles')
        .where({id: roleId});

    if (role?.length === 0) {
        return res.status(404).json({error: `Role with id ${id} not found`});
    }

    let user = await db('users').select('email').where({email: email})

    if (user?.length > 0) {
        return res.status(400).json({error: "User with the same email was found"})
    }

    const randomPassword = Math.random().toString(36).slice(-6);
    const hash = bcrypt.hashSync(randomPassword);

    const username = `${name.toLocaleLowerCase().trim()}.${lastName.toLocaleLowerCase().trim()}`

    const insertedMemberId = await trx('communications')
        .insert({name, lastName, email}).returning('id')

    const communicationId = insertedMemberId[0]

    user = await trx('users')
        .insert({hash, email, username, communicationId, roleId})
        .returning("*")
        .catch(() => {
            trx.rollback();
            res.status(500).json({error: "An Error Occurred during user creation"})
        })

    if (user?.length === 0) {
        trx.rollback();
        return res.status(400).json("Failed to Save new User")
    }

    const subject = "Access Credentials"
    const htmlTemplate =
        `
        <div style="text-align:center; background-color:#F8F9F9">
            <p style="color: green">WELCOME TO COMMUNICATIONS PLATFORM</p>
            <hr/>
            <p style="font-weight: bold">You were added to Communication Platform From Maputo Port as Admin.</p>
            <p>Access Credentials</p> 
            <p>Username:${name.toLocaleLowerCase().trim()}.${lastName.toLocaleLowerCase().trim()}</p>
            <p>Password: ${randomPassword}</p>
            <hr/>
            <p>Change your password after the first login</p>
        </div>
    `

    const EmailSender = new SendEmail();
    try {
        await EmailSender.emailSender(
            [email],
            subject,
            `User: ${name}.${lastName}; Password: ${randomPassword}`,
            null,
            htmlTemplate,
            '')
    } catch (e) {
        trx.rollback();
        return res.status(500).json({error: `Failed to Create user`});
    }

    //Build a worker to resolver sent email Response

    await trx.commit();

    return res.status(200).json({...user[0], hash: ""});

}


module.exports = createUser;
