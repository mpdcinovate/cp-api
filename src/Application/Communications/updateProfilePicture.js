const fs = require('fs')

const UpdateProfilePicture = async (req, res, db) => {
    const {encodedImage} = req.body;
    const {communicationId} = req.params;

        if (encodedImage) {
            let base64Image = encodedImage.split(';base64,').pop();
            const fileName = `${Date.now()}-image.png`

            fs.writeFile(`./uploads/${fileName}`, base64Image, {encoding: 'base64'}, function (err) {
                console.log(err);
            });

            db('communications')
                .returning('*')
                .where({id: communicationId})
                .update({pictureUrl: fileName})
                .returning('*')
                .then(result => res.json(result[0]))
                .catch(err => res.status(400).json("Profile picture change failed!"))
        }
        else {
            return res.status(400).json({error: 'request not containing encoded image'})
        }
};

module.exports = UpdateProfilePicture;
