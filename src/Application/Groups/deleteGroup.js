const deleteGroup = async (req, res, db) => {
    const {id} = req.params;

    const groups = await db('groups')
        .where('id', id)
        .del()
        .returning("*")

    if(groups?.length === 0){
        return res.status(404).json({error: `Group does not exits`})
    }

    return res.json(groups[0]);
}

module.exports = deleteGroup;
