const listGroups = async (res, db) => {
    await db('groups')
        .select('*')
        .then(data => res.json(data))
        .catch(err => res.status(400).json(err))
}

module.exports = listGroups;
