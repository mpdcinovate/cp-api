const getGroupById = async (req, res, db) => {
    const { id } = req.params

    const group = await db('groups')
        .where('id', id)

    if(group?.length === 0){
        return res.status(404).json({error: `Group does not exits`})
    }

    const groupContacts = await db('groupContacts')
        .where('groupId', id)
        .select('contactsId');

    if(groupContacts?.length === 0){
        return res.status(404).json({error: `Group without members!!!`})
    }

    const contactsIds = groupContacts?.map(contact => contact.contactsId)

    const contacts = await db('contacts').whereIn('id',contactsIds);

    return res.json({...group[0], contacts});
}

module.exports = getGroupById;
