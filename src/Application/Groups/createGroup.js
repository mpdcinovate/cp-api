const createGroup = async (req, res, db) => {
    const {groupName, description, contactsIds} = req.body;

    let group = await db('groups').where({name: groupName})

    if (group?.length > 0) {
        return res.status(400).json({error: `Group with name ${groupName} already exists`})
    }

    if(contactsIds?.length < 2 ){
        return res.status(400).json({error: `A group must have more than one member`})
    }




    const transaction = await db.transaction();

    group = await transaction?.insert({
        name: groupName,
        description
    })
        .into('groups')
        .returning('*')
        .catch(err => {
            transaction?.rollback();
            return res.status(400).json({error: err})
        });

    if (!group?.length || group?.length === 0) {
        transaction?.rollback();
        return res.status(400).json({error: 'Failed to create group'})
    }

    const groupMembers = contactsIds?.map(id => ({groupId: group[0].id, contactsId: id}))

    transaction('groupContacts')
        .insert(groupMembers)
        .then(() => {
            transaction?.commit();
            return res.json(group)
        })
        .catch(error => {
            transaction?.rollback();
            return res.status(400).json({error: `Failed to store group members with error ${error}`})
        })
}

module.exports = createGroup;
