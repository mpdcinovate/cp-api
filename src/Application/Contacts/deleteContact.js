const deleteContact = async (req, res, db) => {
    const { id } = req.params;

    const contact = await db('contacts')
        .where({ id });

    if (contact?.length === 0) {
        return res.status(404).json({ error: 'contact not found' })
    }

    const groupContacts = await db('groupContacts')
        .where('contactsId', id)

    if (groupContacts?.length > 0) {
        const group = await db('groups')
            .where('id', groupContacts[0].groupId)

        return res.status(400).json({ error: `The contact belongs to group called: ${group[0].name}. First you should delete the contact in the group.` })
    }

    await db('contacts')
        .where({ id })
        .del()
        .catch(err => res.status(400).json({ error: err }))

    return res.json(contact[0])
}

module.exports = deleteContact;
