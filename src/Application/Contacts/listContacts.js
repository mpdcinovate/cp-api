const listContacts = async (req, res, db) => {
    return res.json(await db('contacts').returning('*'))

}

module.exports = listContacts;
