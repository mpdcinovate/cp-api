const updateContact = async (req, res, db) => {
    const { id, name, lastName, gender, email, phone, department, company, designation } = req.body

    const contact = await db('contacts')
        .where({ id })
        

    if (contact?.length === 0) {
        return res.status(404).json({ error: 'contact not found' })
    }

    return res.json(await db('contacts')
        .where({ id })
        .update({
            name,
            lastName,
            gender,
            email,
            phone,
            department,
            company,
            designation
        })
        .returning('*'))

}

module.exports = updateContact;

