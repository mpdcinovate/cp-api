const getContactsByType = async (req, res, db) => {
    const {type} = req.params

    if (type === 'all') {
        await db('contacts')
            .select('id', 'name', 'lastname', 'email', 'phone')
            .then(data => res.json(data))
    }
    if (type === 'intern') {
        const idContacts = await db('internalContacts').select('contactId')
        const ids = idContacts?.map(id => id.contactId)
        
        await db('contacts')
            .select('id', 'name', 'lastname', 'email', 'phone')
            .whereIn('id', ids)
            .then(data => res.json(data))
    }

    if (type === 'extern') {
        const idContacts = await db('externalContacts').select('contactId')
        const ids = idContacts?.map(id => id.idContacts)

        await db('contacts')
            .select('id', 'name', 'lastname', 'email', 'phone')
            .whereIn('id', ids)
            .then(data => res.json(data))
    }

}

module.exports = getContactsByType;
