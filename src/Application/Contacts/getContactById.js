const getContactById = async (req, res, db) => {
    const { id } = req.params

    return res.json(await db('contacts').where({id}))
}

module.exports = getContactById;
