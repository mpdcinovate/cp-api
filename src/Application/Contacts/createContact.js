const createContact = async (req, res, db) => {

    let {contacts, groupId, groupName} = req.body

    const transaction = await db.transaction();

    let existingContactMessage = '';
    for (const contact of contacts) {
        if (!contact.department && !contact.company) {
            return res.status(400).json({error: 'Cannot save a contact with both department and company empty'})
        }

        const validateContact = await transaction('contacts')
            .where({email: contact.email})
            .orWhere({phone: contact?.phone})
            .returning('*')
            .catch(err => console.log(err))


        if (validateContact?.length > 0) {
            existingContactMessage += `${contact?.name} ${contact?.lastname}, `
        }
    }

    if (existingContactMessage === '') {
        const newContacts = await transaction('contacts')
            .insert(contacts)
            .returning('*');

        if (groupId || groupName) {
            if (groupName) {
                let group = await transaction('groups').where({name: groupName})

                if (group?.length > 0) {
                    transaction.rollback();
                    return res.status(400).json({error: `Group with name ${groupName} already exists`})
                }

                group = await transaction?.insert({
                    name: groupName,
                    description: 'Sem Descrição'
                }).into("groups").returning('*')

                if (group?.length > 0) {
                    groupId = group[0].id;
                } else {
                    transaction.rollback()
                    return res.status(500).json({error: `Failed to add new group`})
                }


            }

            const membersToAdd = newContacts?.map(contact => ({groupId, contactsId: contact.id}))
            const groupContacts = await transaction('groupContacts').insert(membersToAdd).returning("*");

            if (groupContacts?.length > 0) {
                await transaction.commit();
                return res.status(201).json(newContacts)
            } else {
                transaction.rollback()
                return res.status(500).json({error: `Failed to add contacts to group`})
            }
        }

    } else {
        await transaction.rollback();
        return res.status(400).json({error: `${existingContactMessage} already exists`})
    }
}
module.exports = createContact;

