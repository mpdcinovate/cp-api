const setToDefaultAlertMessage = async (req, res, db) => {
    const {id} = req.params

    const trx = await db.transaction();

    const alertMessage = await trx('alertMessages')
        .where({id})
        .returning('*')
        .update({
            default: true
        })
        .catch(err => res.status(400).json({error: err}))


    await trx('alertMessages')
        .whereNot({id})
        .update({
            default: false
        })
        .catch(err => res.status(400).json({error: err}))

    await trx?.commit()

    return res.json(alertMessage[0])
}

module.exports = setToDefaultAlertMessage;
