const createAlertMessage = async (req, res, db) => {
    const {name, body, defaultMessage} = req.body

    const alertMessage = await db('alertMessages')
        .where({name});

    if (alertMessage?.length > 0) {
        return res.status(404).json({error: `Alert Message with name '${name}' already exists. Try another`})
    }

    if (!defaultMessage) {
        await db('alertMessages')
            .insert({
                name,
                body,
                default: defaultMessage
            })
            .returning('*')
            .then(result => res.json(result[0]))
            .catch(err => res.status(400).json({error: err}))
    } else {
        const trx = await db.transaction();

        const createdAlertMessage = await trx('alertMessages')
            .insert({
                name,
                body,
                default: defaultMessage
            })
            .returning('*')
            .catch(err => res.status(400).json({error: err}))


        await trx('alertMessages')
            .whereNot({id: createdAlertMessage[0].id})
            .update({
                default: false
            })
            .catch(err => res.status(400).json({error: err}))


        await trx?.commit()

        return res.status(200).json(createdAlertMessage[0])
    }
}

module.exports = createAlertMessage;
