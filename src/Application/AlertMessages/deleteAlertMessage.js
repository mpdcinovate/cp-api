const deleteAlertMessage = async (req, res, db) => {
    const {id} = req.params;
    const alertMessage = await db('alertMessages')
        .where({id: id})

    if(alertMessage?.length === 0){
        return res.status(404).json({error: `Alert Message Not Found`})
    }

    await db('alertMessages').where('id', '=', id).del().returning("*")
        .then(response => res.json(response[0]))
        .catch((err) => res.status(400).json({error: `Failed to delete Alert Message. Error: ${err}`}))
}

module.exports = deleteAlertMessage;
