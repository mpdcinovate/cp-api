const listAlertMessages = async (req, res, db) => {

    await db('alertMessages')
        .select('*')
        .then(result => res.json(result))
        .catch(err => res.status(400).json({error: err}))
}

module.exports = listAlertMessages;
