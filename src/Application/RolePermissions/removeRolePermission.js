module.exports = async function removeRolePermission(request, response, db){
    const {id} = request.params;

    let rolePermission = await db('rolePermissions')
        .where({id});

    if(rolePermission?.length === 0){
        return response.status(404).json({error: `Role permission not found`});
    }

    rolePermission = await db('rolePermissions')
        .where({id})
        .del()
        .returning('*');

    if(rolePermission?.length > 0){
        return response.status(200).json(rolePermission[0]);
    }

    return response.status(500).json({error: `An error occurred while removing the role permission`});



}
