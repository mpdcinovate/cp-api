module.exports = async function createRolePermission(request, response, db) {
    const {roleId, permissionId} = request.body;

    const role = await db('roles')
        .where({id});

    if(role?.length === 0){
        return response.status(404).json({error: `Role with id ${id} not found`});
    }

    const permission = await db('permissions')
        .where({id});

    if(permission?.length === 0){
        return response.status(404).json({error: `Permission with Id ${id} not found`});
    }

    const rolePermission = await db('rolePermissions')
        .insert({
            roleId,
            permissionId
        })
        .returning('*');

    if(rolePermission?.length > 0){
        return response.status(201).json(rolePermission[0]);
    }

    return response.status(500).json({error: `Failed to create permission`});
}
