const MAX = 999999
const MIN = 100000
const FROM = 'communications@portmaputo.com'
const SUBJECT = 'Recuperação de Acesso'
const TIMEOUT = 1800000 //caso a pessoa nao recupere o acesso dentro de 30min
const sendEmail = require('../../Infrastructure/Mailer/EmailService')

const getRecoveryCodeByEmail = async (req, res, db) => {
    const {email} = req.body

    const data = await db('users')
        .select('*')
        .where('email', '=', email)
        .catch(err => {
            return res.status(400).json({error: err})
        });

    if (data?.length !== 0) {

        const random = Math.floor(Math.random() * (MAX - MIN)) + MIN;

        return db('users')
            .where({email: email})
            .update({recoveryCode: random})
            .then(async () => {

                await sendEmail(
                    email,
                    FROM,
                    SUBJECT,
                    `Olá ${email}, \n Recupere o teu acesso ao PORT COMMUNICATIONS através do código ${random} \n #Por favor, realize esta ação dentro de 30minutos`,
                    [],
                    ""
                )

                setTimeout(async () => {
                    const data = await db('users').select('recoveryCode').where('email', '=', email)

                    if (data[0].recoveryCode !== 0) {
                        db('users')
                            .where({email: email})
                            .update({recoveryCode: ''})
                            .then(() => {
                                sendEmail(
                                    email,
                                    FROM,
                                    SUBJECT,
                                    `Olá ${email}, \n Infelizmente, o tempo de recuperação de acesso expirou. Por favor, volte a pedir o código de recuperação aqui: http://localhost:3000/recuperar-acesso`,
                                    [],
                                    ""
                                )
                            })
                    }
                }, TIMEOUT);

                return res.json('exist')
            })
            .catch(() => res.status(400).json({error: "Failed to Verify Email"}))

    } else {
        res.status(400).json({error: `User with email ${email} doesn't exist`})
    }
}

module.exports = getRecoveryCodeByEmail;
