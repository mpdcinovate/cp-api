const bcrypt = require("bcrypt-nodejs");

const changePassword = async (req, res, db) => {
    const { oldPassword, newPassword } = req.body;
    const { communicationId } = req.params;

    const userHash = await db('users')
        .select('hash').where({ communicationId })
        .then(result => result[0])
        .catch(err => res.status(400).json({ error: err }));

    if (!userHash?.hash) {
        return res.status(404).json({ error: "User Not Found!" });
    }

    const isValid = await bcrypt.compareSync(oldPassword, userHash.hash);

    if (!isValid) {
        return res.status(401).json({ error: "The Current Password you entered is not correct." })
    }

    const hash = bcrypt.hashSync(newPassword);

    const user = await db('users')
        .where({ communicationId })
        .update({ hash: hash })
        .returning("*")
        .catch(err => res.status(400).json({ error: err }));

    return res.status(200).json({ ...user[0], hash: "" })
}

module.exports = changePassword;