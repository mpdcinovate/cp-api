checkAccessRecoveryCode = async (req, res, db) => {
    const {recoveryCode} = req.body

    const data = await db('users')
        .select('*')
        .where('recoveryCode', '=', recoveryCode)
        .catch(err => {
            return res.status(400).json({error: err})
        });

    if (data?.length !== 0) {
        return db('users')
            .where({recoveryCode: recoveryCode})
            .update({recoveryCode: 0})
            .then(() => res.status(200).json('Successfully verified your identification. You Can, now, reset your password'))
            .catch(() => res.status(400).json(''))
    } else {
        return res.status(400).json({error: 'recovery code expired or you entered an incorrect code!'})
    }
}

module.exports = checkAccessRecoveryCode;
