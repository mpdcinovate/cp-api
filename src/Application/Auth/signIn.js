const jwtGen = require('../../Infrastructure/Security/generateJwt')
const signIn = async (req, res, db, bcrypt) => {
    const {username, password} = req.body;

    try {
        if (!username || !password) {
            return res.status(400).json('incorrect form submission');
        }

        const usersWithEnteredUserName = await db('users').select('email', 'hash')
            .where('username', '=', username)

        if (usersWithEnteredUserName?.length === 0) {
            return res.status(401).json({error: 'you entered wrong credentials'});
        }

        let confirmedUserEmail = '';
        usersWithEnteredUserName?.map(user => {
            const isValid = bcrypt.compareSync(password, user.hash)
            if (isValid) {
                confirmedUserEmail = user.email
            }
        })

        if (confirmedUserEmail !== '') {
            const confirmedUserData = await db('communications')
                .join('users', 'communications.id', '=', 'users.communicationId')
                .select(
                    'communications.id',
                    'communications.email',
                    'communications.name',
                    'communications.lastName',
                    'communications.super',
                    'communications.pictureUrl',
                    'users.roleId')
                .where('communications.email', '=', confirmedUserEmail)

            const role = await db('roles')
                .select('*').where({id: confirmedUserData[0].roleId})

            const rolePermissionsIds = await db('rolePermissions')
                .select('permissionId').where({roleId: confirmedUserData[0].roleId})

            const permissions = await db('permissions')
                .select('code').whereIn('id', rolePermissionsIds.map(rolePermission => rolePermission.permissionId));


            const user = {
                email: confirmedUserData[0].email,
                name: confirmedUserData[0].name,
                lastName: confirmedUserData[0].lastName,
                super: confirmedUserData[0].super,
                id: confirmedUserData[0].id,
                profilePicture: confirmedUserData[0].pictureUrl,
                roleId: role[0].id,
                roleName: role[0].name,
                permissions: permissions.map(permission => permission.code).join()
            };

            return res.json(jwtGen(user))
        } else {
            return res.status(401).json({error: 'you entered wrong credentials'});
        }
    } catch (e) {
        return res.status(400).json({error: e.toString()});
    }
}

module.exports = signIn;
