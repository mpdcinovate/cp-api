const bcrypt = require("bcrypt-nodejs");

const resetUserPassword = async (req, res, db) => {
    const {email, password} = req.body

    const hash = bcrypt.hashSync(password);

    try {
        const user = await db('users')
            .where({email: email})
            .returning('*')

        if (user?.length === 0) {
            return res.status(404).json({error: "User Not Found!"});
        }

        if(parseInt(user[0]?.recoveryCode) !== 0){
            return res.status(404).json({error: "Ask for you Recovery Code to recover you access!"});
        }

        db('users')
            .where({email: email})
            .update({hash: hash, recoveryCode: ''})
            .returning('*')
            .then(result => res.json({...result[0], hash: ''}))

    } catch (e) {
        return res.status(400).json({error: e.message})
    }


}

module.exports = resetUserPassword;
