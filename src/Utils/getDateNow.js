module.exports = function getDateNow() {
    const dateNow = new Date(Date.now());
    dateNow.setHours(dateNow.getHours() + 2);

    return dateNow;
}
