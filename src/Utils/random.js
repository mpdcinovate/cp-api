const MAX = 999999
const MIN = 100000

module.exports = class Random {
    random() {
        return Math.floor(Math.random() * (MAX - MIN)) + MIN;
    }
}

