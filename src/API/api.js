const express = require('express');
const http = require('http');
const app = express();
const cors = require('cors');
const path = require('path')
const { appRoutes } = require("./Routes/index")
const jobs = require('./Workers/index');

const PORT = 3333;

const server = http.createServer(app)

app.use(express.urlencoded({ extended: true }));
app.use(express.json());
app.use(cors());
app.use('/api/files', express.static(path.resolve(__dirname, '../../uploads')));

app.use(appRoutes)
jobs.scheduledMessagesJob();

server.listen(PORT, () => console.log("LISTEN TO PORT", PORT))


