const express = require('express');
const router = express.Router();
const {db} = require("../../Persistence/connection")
const authenticateToken = require('../../Infrastructure/Security/authenticateToken')

const permissions = require('../../Application/Permissions')

router.post('/', authenticateToken, (req, res) => permissions.create(req, res, db))
router.get('/', authenticateToken, (req, res) => permissions.list(req, res, db))
router.get('/:id', authenticateToken, (req, res) => permissions.getById(req, res, db))
router.delete('/:id', authenticateToken, (req, res) => permissions.remove(req, res, db))
router.put('/:id', authenticateToken, (req, res) => permissions.update(req, res, db))

module.exports = router;
