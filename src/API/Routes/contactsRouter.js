const express = require('express');
const router = express.Router();
const {db} = require("../../Persistence/connection")

const createContact = require('../../Application/Contacts/createContact')
const listContacts = require('../../Application/Contacts/listContacts')
const getContactById = require('../../Application/Contacts/getContactById')
const getContactByType = require('../../Application/Contacts/getContactsByType')
const updateContact = require('../../Application/Contacts/updateContact')
const deleteContact = require('../../Application/Contacts/deleteContact')

router.post('/',(req, res) => createContact(req,res,db));
router.get('/',(req, res) => listContacts(req,res,db));
router.get('/:id',(req, res) => getContactById(req,res,db));
router.get('/:type/ByType',(req, res) => getContactByType(req,res,db));
router.put('/',(req, res) => updateContact(req,res,db));
router.delete('/:id',(req, res) => deleteContact(req,res,db));


module.exports = router;
