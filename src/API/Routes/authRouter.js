const express = require('express');
const router = express.Router();
const bcrypt = require("bcrypt-nodejs");
const { db } = require("../../Persistence/connection")
const authenticateToken = require('../../Infrastructure/Security/authenticateToken')

const signIn = require('../../Application/Auth/signIn')
const changePassword = require('../../Application/Auth/changePassword')
const verifyUserByEmail = require('../../Application/Auth/getRecoveryCodeByEmail')
const checkAccessCode = require('../../Application/Auth/confirmRecoveryCode')
const resetUserPassword = require('../../Application/Auth/resetUserPassword')

router.post("/", (req, res) => signIn(req, res, db, bcrypt));
router.put("/:communicationId/passwordChange", authenticateToken, (req, res) => changePassword(req, res, db));
router.put("/verifyEmail", (req, res) => verifyUserByEmail(req, res, db));
router.put("/checkAccessCode", (req, res) => checkAccessCode(req, res, db));
router.put("/resetUserPassword", (req, res) => resetUserPassword(req, res, db));

module.exports = router;
