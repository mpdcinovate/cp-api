const express = require('express');
const router = express.Router();
const EWS = require('node-ews')
const {db} = require("../../Persistence/connection")
const serviceCredentials = require('../../Application/Emails/exchangeConnection')
const authenticateToken = require('../../Infrastructure/Security/authenticateToken')

//Getting MailBox Credentials
let ews = ''
serviceCredentials(db).then(result => {
    const ewsConfig = {...result[0], auth: 'basic'};
    ews = new EWS(ewsConfig)
});

const listMailBoxMessages = require('../../Application/Emails/listMailBoxMessages')
const getInboxMessageById = require('../../Application/Emails/getInboxMessageById')
const setMessageToSeen = require('../../Application/Emails/setMessageToSeen')
const getEmailsGroupByCompany = require('../../Application/Emails/getEmailsGroupByCompany')


router.get('/', /*authenticateToken,*/ (req, res) => listMailBoxMessages(req, res, db, ews))
router.get('/:id', /*authenticateToken,*/ (req, res) => getInboxMessageById(req, res, db))
router.put('/Message/:id', /*authenticateToken,*/ (req, res) => setMessageToSeen(req, res, db))
router.get('/Emails/Company', /*authenticateToken,*/ (req, res) => getEmailsGroupByCompany(req, res, db))

module.exports = router;
