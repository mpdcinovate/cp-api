const express = require('express');
const router = express.Router();
const {db} = require("../../Persistence/connection")
const authenticateToken = require('../../Infrastructure/Security/authenticateToken')

const roles = require('../../Application/Roles')

router.post('/', authenticateToken, (req, res) => roles.create(req, res, db))
router.get('/', authenticateToken, (req, res) => roles.list(req, res, db))
router.get('/:id', authenticateToken, (req, res) => roles.getById(req, res, db))
router.delete('/:id', authenticateToken, (req, res) => roles.remove(req, res, db))
router.put('/:id', authenticateToken, (req, res) => roles.update(req, res, db))

module.exports = router;
