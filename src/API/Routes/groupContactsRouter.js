const express = require('express');
const router = express.Router();
const {db} = require("../../Persistence/connection")

const addGroupContacts = require('../../Application/GroupContacts/addGroupContacts')
const deleteGroupContacts = require('../../Application/GroupContacts/deleteGroupContacts')
const listGroupContactsByGroupId = require('../../Application/GroupContacts/listGroupContactsByGroupId')
const listFreeContacts = require('../../Application/GroupContacts/listFreeContacts')


router.post('/', (req, res) => addGroupContacts(req, res, db))
router.delete('/', (req, res) => deleteGroupContacts(req, res, db))
router.get('/ByGroup/:groupId', (req, res) => listGroupContactsByGroupId(req, res, db))
router.get('/freeContacts/:groupId', (req, res) => listFreeContacts(req, res, db))

module.exports = router;
