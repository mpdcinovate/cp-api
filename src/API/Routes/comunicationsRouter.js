const express = require('express');
const router = express.Router();
const { db } = require("../../Persistence/connection")
const authenticateToken = require('../../Infrastructure/Security/authenticateToken')

const getUsers = require('../../Application/Communications/getUsers')
const createUsers = require('../../Application/Communications/createUser')
const getById = require('../../Application/Communications/getCommunicationsById')
const deleteCommunication = require('../../Application/Communications/deleteCommunication')
const updateCommunication = require('../../Application/Communications/updateCommunicationUser')
const updateProfilePicture = require('../../Application/Communications/updateProfilePicture')
const setSuperUser = require('../../Application/Communications/setSuperUser')
const unsetSuperUser = require('../../Application/Communications/unsetSuperUser')

router.get("/", (req, res) => getUsers(res, db));
router.post("/", (req, res) => createUsers(req, res, db));
router.get("/:id", (req, res) => getById(req, res, db));
router.delete("/:id", (req, res) => deleteCommunication(req, res, db));
router.put("/:id", (req, res) => updateCommunication(req, res, db));
router.put("/:communicationId/ProfilePicture", authenticateToken, (req, res) => updateProfilePicture(req, res, db));
router.put("/:communicationId/setSuper", (req, res) => setSuperUser(req, res, db));
router.put("/:communicationId/unsetSuper", (req, res) => unsetSuperUser(req, res, db));

// router.post('/create_communications',

//     (req, res) => {
//         communications.create(req, res, db, bcrypt)
//     })
//
// router.get('/show_communications/:id', (req, res) => {
//     communications.show(req, res, db)
// })
//
// router.get('/index_communications', (req, res) => {
//     communications.index(req, res, db)
// })
//
// router.delete('/delete_communications/:id', (req, res) => {
//     communications.delete(req, res, db)
// })
//
// router.put('/super_communications/:id', (req, res) => {
//     communications.super(req, res, db)
// })

module.exports = router;
