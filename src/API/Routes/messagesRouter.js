const express = require('express');
const router = express.Router();
const multer = require('multer');
const {db} = require("../../Persistence/connection")
const authenticateToken = require('../../Infrastructure/Security/authenticateToken')

const sendMessage = require('../../Application/Messages/sendMessage')
const listMessages = require('../../Application/Messages/listMessages')
const scheduleMessage = require('../../Application/Messages/scheduleMessage')

const storage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, 'uploads')
    },
    filename: function (req, file, cb) {
        cb(null, Date.now() + '-' + file.originalname)
    }
});

const upload = multer({storage: storage}).array('file');

function saveDocument(req, res, next) {

    upload(req, res, function (err) {
        if (err instanceof multer.MulterError) {
            return res.status(500).json(err)
        } else if (err) {
            return res.status(500).json(err)
        }
        next();
    })

}

router.post('/', saveDocument, authenticateToken, (req, res) => sendMessage(req, res, db))
router.get('/', /*authenticateToken,*/ (req, res) => listMessages(req, res, db))
router.post('/schedule', saveDocument, authenticateToken, (req, res) => scheduleMessage(req, res, db))


module.exports = router;
