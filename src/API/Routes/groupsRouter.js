const express = require('express');
const router = express.Router();
const {db} = require("../../Persistence/connection")

const createGroup = require('../../Application/Groups/createGroup')
const listGroups = require('../../Application/Groups/listGroups')
const getGroupById = require('../../Application/Groups/getGroupById')
const deleteGroup = require('../../Application/Groups/deleteGroup')

router.post('/', (req, res) => createGroup(req, res, db))
router.get('/', (req, res) => listGroups(res, db))
router.get('groupBy/:id', (req, res) => getGroupById(req, res, db))
router.delete('/:id', (req, res) => deleteGroup(req, res, db))


module.exports = router;
