const express = require('express');
const appRoutes = express();

const communicationsRouter = require("./comunicationsRouter")
const authRouter = require("./authRouter")
const alertMessagesRouter = require("./alertMessagesRouter")
const contactsRouter = require("./contactsRouter")
const groupsRouter = require("./groupsRouter")
const groupContactsRouter = require("./groupContactsRouter")
const messageRouter = require("./messagesRouter")
const publicationsRouter = require("./publicationsRouter")
const publicationCommentsRouter = require("./publicationCommentsRouter")
const mailBoxRouter = require("./MailBoxRouter")
const rolesRouter = require("./rolesRouter")
const permissionsRouter = require("./permissionRouter")

appRoutes.use("/api/communications", communicationsRouter);
appRoutes.use("/api/auth", authRouter)
appRoutes.use("/api/alertMessages", alertMessagesRouter)
appRoutes.use("/api/contacts", contactsRouter)
appRoutes.use("/api/groups", groupsRouter)
appRoutes.use("/api/groupContacts", groupContactsRouter)
appRoutes.use("/api/messages", messageRouter)
appRoutes.use("/api/publications", publicationsRouter)
appRoutes.use("/api/publicationComments", publicationCommentsRouter)
appRoutes.use("/api/mailBox", mailBoxRouter)
appRoutes.use("/api/roles", rolesRouter)
appRoutes.use("/api/permissions", permissionsRouter)

module.exports = {
    appRoutes: appRoutes
}
