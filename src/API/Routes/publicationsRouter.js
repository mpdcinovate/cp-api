const express = require('express');
const router = express.Router();
const multer = require('multer');
const { db } = require("../../Persistence/connection")
const authenticateToken = require('../../Infrastructure/Security/authenticateToken')

const createPublication = require('../../Application/Publications/createPublication')
const listPublications = require('../../Application/Publications/listPublications')
const getPublicationById = require('../../Application/Publications/getPublicationById')
const deletePublication = require('../../Application/Publications/deletePublication')

var storage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, 'uploads')
    },
    filename: function (req, file, cb) {
        cb(null, Date.now() + '-' + file.originalname)
    }
})

var upload = multer({ storage: storage }).array('file')

function saveDocument(req, res, next) {

    upload(req, res, function (err) {
        if (err instanceof multer.MulterError) {
            return res.status(500).json(err)
        } else if (err) {
            return res.status(500).json(err)
        }
        next();
    })

}

router.post('/', saveDocument, (req, res) => createPublication(req, res, db));
router.get('/', (req, res) => listPublications(req, res, db))
router.get('/:id', authenticateToken, (req, res) => getPublicationById(req, res, db))
router.delete('/:id', authenticateToken, (req, res) => deletePublication(req, res, db))


module.exports = router;
