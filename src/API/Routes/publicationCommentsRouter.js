const express = require('express');
const router = express.Router();
const {db} = require("../../Persistence/connection")
const authenticateToken = require('../../Infrastructure/Security/authenticateToken')

const createComment = require('../../Application/PostComments/createComment')
const listComments = require('../../Application/PostComments/listComments')
const listIndexedComments = require('../../Application/PostComments/listIndexedComments')
const markAsSeenComment = require('../../Application/PostComments/markAsSeenComment')
const replyToComment = require('../../Application/PostComments/replyToComment')
const getCommentResponsesByCommentId = require('../../Application/PostComments/getCommentResponsesByCommentId')

router.post('/', (req, res) => createComment(req, res, db))
router.get('/', authenticateToken, (_, res) => listComments(res, db))
router.get('/indexed', /*authenticateToken,*/ (req, res) => listIndexedComments(req, res, db))
router.put('/CommentOpen', /*authenticateToken,*/ (req, res) => markAsSeenComment(req, res, db))
router.put('/replyComment', authenticateToken, (req, res) => replyToComment(req, res, db))
router.get('/responses/:id', authenticateToken, (req, res) => getCommentResponsesByCommentId(req, res, db))

module.exports = router;
