const express = require('express');
const router = express.Router();
const {db} = require("../../Persistence/connection")

const createAlertMessage = require('../../Application/AlertMessages/createAlertMessage')
const listAlertMessages = require('../../Application/AlertMessages/listAlertMessages')
const setToDefaultAlertMessage = require('../../Application/AlertMessages/setToDefaultAlertMessage')
const deleteAlert = require('../../Application/AlertMessages/deleteAlertMessage')

router.post('/', (req, res)=>createAlertMessage(req,res,db));
router.get('/', (req, res)=>listAlertMessages(req,res,db));
router.put('/:id', (req, res)=>setToDefaultAlertMessage(req,res,db));
router.delete('/:id', (req, res)=>deleteAlert(req,res,db));

module.exports = router;

