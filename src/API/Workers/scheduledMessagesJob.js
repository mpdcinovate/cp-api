const {db} = require('../../Persistence/connection')
const cron = require('node-cron')
const getDateNow = require('../../Utils/getDateNow')
const EmailService = require('../../Infrastructure/Mailer/EmailService')
const TwilioSms = require('../../Infrastructure/TwilioSms/TwilioSmsService')

module.exports = function scheduledMessagesJob() {
    cron.schedule('*/1 * * * *', async () => {
        let emailAddresses = [], phoneNumbers = [], attachments = [];

        const messages = await db('messages')
            .returning('*')
            .where({alreadySent: false});

        if (messages?.length > 0) {
            for (const message of messages) {
                try {
                    const date = new Date(Date.now());
                    date.setHours(date.getHours() + 2);
                    /*let actualScheduledDate = new Date(message.scheduledTo);
                    actualScheduledDate.setHours(date.getHours() - 2);*/

                    if (date >= message.scheduledTo) {
                        const messageRecipients = await db('messageRecipients')
                            .where({messageId: message.id});

                        const individualRecipients = messageRecipients.filter(recipient => recipient.groupName === null);

                        // Composing individual recipient contacts
                        if (individualRecipients?.length > 0) {
                            const emails = individualRecipients
                                .filter(rec => rec.contactEmail)
                                .map(recipient => recipient.contactEmail);

                            if (emails?.length > 0) {
                                emailAddresses.push(...emails);
                            }

                            const numbers = individualRecipients
                                .filter(rec => rec.contactPhone)
                                .map(recipient => recipient.contactPhone);

                            if (numbers?.length > 0) {
                                phoneNumbers.push(...numbers);
                            }
                        }

                        const groupRecipients = messageRecipients.filter(recipient => recipient.groupName !== null);

                        // Composing the group recipients
                        if (groupRecipients?.length > 0) {
                            const groupNames = groupRecipients.map(groupRecipient => groupRecipient.groupName);

                            const groups = await db('groups')
                                .select('id')
                                .whereIn('name', groupNames);

                            const groupIds = groups.map(group => group.id);

                            const contactsInGroups = await db('groupContacts')
                                .select('contactsId')
                                .whereIn('groupId', groupIds)

                            // Getting contacts ids from group and avoiding their duplications with "new Set"...
                            const groupContactsIds = [...new Set(contactsInGroups?.map(contactId => contactId.contactsId))];

                            const contacts = await db('contacts')
                                .select('email', 'phone')
                                .whereIn('id', groupContactsIds)

                            const emails = contacts
                                .filter(contact => contact.email !== null)
                                .map(cont => cont.email);

                            if (emails?.length > 0) {
                                emailAddresses.push(...emails);
                            }

                            const numbers = contacts
                                .filter(contact => contact.phone !== null)
                                .map(cont => cont.phone);

                            if (numbers?.length > 0) {
                                phoneNumbers.push(...numbers);
                            }
                        }

                        const uploadedFiles = await db('messageUploads')
                            .select('name')
                            .where({messageId: message.id});

                        if (uploadedFiles.length > 0) {
                            uploadedFiles.forEach(file => {
                                attachments.push({
                                    filename: file.name.slice(14, file.name.length),
                                    path: `./uploads/${file.name}`
                                });
                            });
                        }

                        if (message.viaEmail && emailAddresses.length > 0) {
                            const emailService = new EmailService();
                            const resp = await emailService.emailSender(
                                emailAddresses,
                                message.subject,
                                message.body,
                                attachments,
                                "",
                                ""
                            );
                        }

                        if (message.viaSms && phoneNumbers.length > 0) {
                            const smsService = new TwilioSms();
                            const resp = await smsService.sendSms(phoneNumbers, message.body);
                        }

                        if (message.alertText && phoneNumbers.length > 0) {
                            const smsService = new TwilioSms();
                            await smsService.sendSms(phoneNumbers, message.alertText);
                        }

                        const updatedMessage = await db('messages')
                            .where({id: message.id})
                            .returning('*')
                            .update({
                                actualSendDate: new Date(Date.now()),
                                alreadySent: true,
                                sent_at: getDateNow()
                            });

                        if (!updatedMessage?.length) {
                            console.log("Failed to update message");
                        }
                    }
                } catch (e) {
                    console.log(e);
                }
            }
        }
    }, {})
}
