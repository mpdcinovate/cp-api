const dotenv = require("dotenv");
dotenv.config();
const path = require("path")

module.exports = {
  client: 'mssql',
  connection: {
    user: process.env.DATABASE_USER,
    password: process.env.ENV === 'prod' ? process.env.DATABASE_REMOTE_PASSWORD : process.env.DATABASE_PASSWORD,
    port: 1433,
    database: process.env.DATABASE_NAME,
    host: process.env.ENV === 'prod' ? process.env.DATABASE_REMOTE_HOST : process.env.DATABASE_HOST,
    options: {
      encrypt: true,
      enableArithAbort: true
    },
  },
    migrations: {
        directory: path.resolve(__dirname, 'src', 'Persistence', 'migrations')
    },
    seeds: {
        directory: path.resolve(__dirname, 'src', 'Persistence', 'seeds')
    },
    useNullAsDefault: true,
}
